import * as React from 'react';
import { StatusBar } from 'react-native'
import { Provider as PaperProvider } from 'react-native-paper';
import { Provider as StoreProvider } from 'react-redux';
import store from './redux/store';

import RootNavigation from './navigation/RootNavigation'


export default App = () => {
  return (
    <StoreProvider store={store}>
      <PaperProvider>
        <StatusBar backgroundColor="#003f3a" />
        <RootNavigation />
      </PaperProvider>
    </StoreProvider>
  );
}