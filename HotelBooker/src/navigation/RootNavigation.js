import * as React from 'react';
import { connect } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator  } from '@react-navigation/bottom-tabs';
import Icons from 'react-native-vector-icons/AntDesign';

import ShoppingCartIcon from '../components/ShoppingCartIcon'

import AuthScreen from '../screens/Auth'
import HomeScreen from '../screens/Home'
import CartScreen from '../screens/Cart' 
import ProfileScreen from '../screens/Profile' 
import RoomTypeScreen from '../screens/RoomType'
import BookingScreen from '../screens/Booking'
import PaymentScreen from '../screens/Payment'
import TransactionScreen from '../screens/Transaction'
import BookingListScreen from '../screens/Booking/List'

const Tab = createBottomTabNavigator ();
const MainTab = (props) => {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          if (route.name === 'Profile') {
            return (
              <Icons
                name={'user'}
                size={size}
                color={color}
              />
            );
          } else if (route.name === 'Home') {
            return (
              <Icons
                name={'home'}
                size={size}
                color={color}
              />
            );
          } else if (route.name === 'BookingList') {
            return (
              <Icons
                name={'calendar'}
                size={size}
                color={color}
              />
            );
          } else if (route.name === 'Transaction') {
            return (
              <Icons
                name={'linechart'}
                size={size}
                color={color}
              />
            );
          }
        },
      })}
      tabBarOptions={{
        activeTintColor: '#003f3a',
        inactiveTintColor: 'gray',
        showLabel: true,
        keyboardHidesTabBar: false,
        tabStyle: {
          paddingVertical: 8,
        },
        style: {
          height: 55,
          elevation: 12,
          borderTopWidth: 0,
        },        
      }}
    >    
      <Tab.Screen name="Home" component={HomeScreen} />
      <Tab.Screen name="BookingList" component={BookingListScreen} />
      <Tab.Screen name="Profile" component={ProfileScreen} />
      {/* <Tab.Screen name="Transaction" component={TransactionScreen} /> */}
    </Tab.Navigator>
  );
}

const RootStack = createStackNavigator();
const RootNavigation = () => {
  return (
    <NavigationContainer>
      <RootStack.Navigator>      
        <RootStack.Screen name="Auth" options={{headerShown:false}} component={AuthScreen} />          
        <RootStack.Screen name="Main" options={{headerTitle:'Hotel Booker', headerLeft:'', headerRight:()=><ShoppingCartIcon/>}} component={MainTab}/>
        <RootStack.Screen name="RoomType" options={{headerTransparent:true, headerTitle:null, headerTintColor:'white'}} component={RoomTypeScreen} />
        <RootStack.Screen name="Booking" component={BookingScreen} />
        <RootStack.Screen name="Cart" options={{headerTitle:'Checkout'}} component={CartScreen} />
        <RootStack.Screen name="Payment" component={PaymentScreen} />
      </RootStack.Navigator>
    </NavigationContainer>
  );
}

export default connect()(RootNavigation)