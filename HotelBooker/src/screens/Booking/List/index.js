
import * as React from 'react'
import { View, Text, Image, ScrollView, TouchableOpacity } from 'react-native'
import { Headline, Caption, Subheading, Snackbar, ActivityIndicator } from 'react-native-paper'
import moment from 'moment'
import Icon from 'react-native-vector-icons/AntDesign' 
import { API_ROUTES, API_METHODS } from '../../../utils/const'
import { fetchAPI } from '../../../utils/API'


import Styles from './styles'

const BookingList = (props) => {
  const [data, setData] = React.useState([{room_type:{},rooms:[],status:''}]);
  const [snackConf, setSnackConf] = React.useState({visible:false,message:''})
  const [loading, setLoading] = React.useState(false)

  React.useLayoutEffect(() => {
    props.navigation.setOptions({
      headerRight: () => (
        deleteAllCartIcon()
      ),
    });
  }, [props.navigation]);

  React.useEffect(() => {
    setLoading(true)
    const fetchData = async () => {
      var response = await fetchAPI(API_METHODS.GET, API_ROUTES.BOOKINGS)
      if(response.success) {
         setData(response.data) 
         setLoading(false)
      } else {
        setSnackConf({visible:true,message:response.message})
        setLoading(false)
      }
    }
    fetchData()
    const unsubscribe = props.navigation.addListener('blur', () => {
      fetchData()
    });
    return unsubscribe;    
  }, [props.navigation]);  

  const deleteAllCartIcon = () => (
    <TouchableOpacity activeOpacity={0.80} onPress={()=>props.removeAllCart()} style={{marginRight:10}}>
      <Icon name="delete" size={30}  color='#a80000'/>
    </TouchableOpacity>
  ) 

  const onResetSnack = () => {
    setSnackConf({visible:false,message:''})
  }

    const bookingItems = (item, i) => {
      var diff = moment(item.book_checkout_date).diff(moment(item.book_checkin_date),'days')
      return (
        // <TouchableOpacity activeOpacity={0.80}>
          <View style={Styles.roomContent} key={i}>
            <View style={Styles.imageRoomWrapper}>
              <Image source={{ uri: item.room_type.image }} style={Styles.imageRoom}/>
            </View>
            <View style={Styles.infoRoomWrapper}>
              <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                <Headline>{item.room_type.name}</Headline>
              </View>
              <Caption>{item.status.toUpperCase()}</Caption>
              <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                <View style={{flexDirection:'row', justifyContent:'space-between', width:'75%'}}>
                  <Subheading>${item.cost}</Subheading>
                  <Subheading>{item.rooms.length}r</Subheading>
                  <Subheading>{diff}d</Subheading>
                  <Subheading>{item.guest_name}</Subheading>
                </View>     
              </View>
              <Caption>{moment(item.book_checkin_date).format('MM/DD/YYYY')+' - '+moment(item.book_checkout_date).format('MM/DD/YYYY')}</Caption>
            </View>
          </View>
        // </TouchableOpacity>
      )
    }
  
  return (
    <>
    {
      loading
      ?  <ActivityIndicator style={{marginTop:20}} animating={true} color={'green'} />
      :  <View style={Styles.container}>
          <ScrollView>
            {
              data.length > 0 
              ? [...data].reverse().map((item, i) => bookingItems(item))
              : <View style={Styles.emptyContainer}><Text>Empty Booking</Text></View>
            }

          </ScrollView>           
          <Snackbar
            visible={snackConf.visible}
            onDismiss={onResetSnack}
            action={{
              label: 'Dismiss',
              onPress: () => onResetSnack(),
            }}
          >
            {snackConf.message}
          </Snackbar>        
        </View>
    }
    </>
  );
}

export default BookingList