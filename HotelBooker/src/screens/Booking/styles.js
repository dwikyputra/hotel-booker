import { hp, wp, statusbarHeight } from '../../utils/responsive'
import {StyleSheet} from 'react-native';


export default {
  container: {
    flex: 1,
  },
  roomContent:{
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'flex-end',
    marginTop: 20
  },
  imageRoomWrapper:{
    width: wp(40),
    paddingLeft: 20,
    marginTop: 5
  },
  imageRoom:{
    width:100,
    height:100,
  },
  infoRoomWrapper:{
    width: wp(60),
    paddingRight:20,
  },
  descRoomWrapper:{
    paddingHorizontal: 20,
  },
  availabilityWrapper:{
    paddingHorizontal: 20,
    marginVertical: 40,
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center' 
  },  
  availabilityItems:{
    marginTop:30,
    flex: 1, 
    flexDirection: 'row'
  },
  footerContainer: {
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  totalCostWrapper: {
    width: wp(40),
    paddingLeft: 15,
    paddingRight: 10,
    paddingVertical: 10,
    justifyContent: 'center',
  },
  sectionLabel: {
    fontSize: 13,
    color: '#666666',
    fontFamily: 'Roboto',
    alignSelf: 'center'
  },
  totalText: {
    fontSize: 30,
    color: '#1a963d',
    fontFamily: 'Roboto-Medium',
    alignSelf: 'center'
  },
  footerButtonWrapper: {
    width: wp(60),
    paddingRight: 30,
    paddingLeft: 10,
    justifyContent: 'center',
  },
  footerButton: {
    backgroundColor: '#003f3a',
  },
  textInputTheme: {
    colors: {
      primary: '#47828f',
      underlineColor: '#47828f',
    },
  }
}