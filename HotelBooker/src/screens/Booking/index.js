
import * as React from 'react'
import { View, Text, Image, ScrollView } from 'react-native'
import { Button, Headline, Caption, Divider, Subheading, TextInput, Avatar, Snackbar  } from 'react-native-paper'
import moment from 'moment'
import { connect } from 'react-redux';
import NumberStepper from 'react-native-custom-stepper'
import { API_ROUTES, API_METHODS } from '../../utils/const'
import { fetchAPI } from '../../utils/API'

import { addItemToCart, editCartItem } from '../../redux/actions'

import Styles from './styles'

const Booking = (props) => {
  const [guestName, setGuestName] = React.useState('');
  const [numRoom, setNumRoom] = React.useState(1);
  const [rooms, setRooms] = React.useState([{room:{}}]);
  const [roomsAvail, setRoomsAvail] = React.useState([]);
  const [isChecked, setIsChecked] = React.useState(false);
  const [checkInDate, setCheckInDate] = React.useState(moment().format('MM/DD/YYYY'));
  const [checkOutDate, setcheckOutDate] = React.useState(moment().add(1,'days').format('MM/DD/YYYY'));
  const [snackConf, setSnackConf] = React.useState({visible:false,message:''});

  const { process_type, cartId, _id, name, subtitle, image, price, startdate, enddate, numroom, guestname } = props.route.params

  const onCheckAvailability = async () => {
    var response = await fetchAPI(API_METHODS.POST, API_ROUTES.ROOM_AVAILABLE, {
      roomtype: _id,
      startdate: moment(checkInDate).format('MM/DD/YYYY'),
      enddate: moment(checkOutDate).format('MM/DD/YYYY'),
    })
    if(response.success) {
      var roomsavail = response.data.filter(r=>r.status!='taken')
      setRoomsAvail(roomsavail)
      setRooms(response.data)
    } 
    setIsChecked(true)
  }

  const onAddToCart = () => {
    console.log(numRoom, roomsAvail.length)
    if(numRoom < roomsAvail.length){
      props.addToCart({
        roomtype: props.route.params,
        startdate: moment(checkInDate).format('MM/DD/YYYY'),
        enddate: moment(checkOutDate).format('MM/DD/YYYY'),
        guestName: guestName,
        numRoom: numRoom
      })
      setSnackConf({visible:true,message:'Added to Cart'})
      props.navigation.navigate('Home',{visible:true,message:'Item added to cart'})
    } else {
      setSnackConf({visible:true,message:'Rooms Available Not Enough'})
    }
  }

  const onEditItemCart = () => {
    props.editItem(cartId, {
      roomtype: props.route.params,
      startdate: moment(checkInDate).format('MM/DD/YYYY'),
      enddate: moment(checkOutDate).format('MM/DD/YYYY'),
      guestName: guestName,
      numRoom: numRoom
    })
    props.navigation.navigate('Cart')
  }

  const onResetSnack = () => {
    setSnackConf({visible:false,message:''})
  }

  React.useEffect(() => {
    if(!guestname==false) setGuestName(guestname)
    if(!numroom==false) setNumRoom(numroom)
    if(!enddate==false) setcheckOutDate(enddate)
    if(!startdate==false) setCheckInDate(startdate)
  }, [props]);

  const footerSection = () => {
    var diff = moment(checkOutDate).diff(moment(checkInDate),'days')
    var totalPrice = price*numRoom*diff
    return (
      <View style={Styles.footerContainer}>
        <View style={Styles.totalCostWrapper}>
          <Text style={Styles.sectionLabel}>Price</Text>
          <Text style={Styles.totalText}>${totalPrice}</Text>
        </View>
        <View style={Styles.footerButtonWrapper}>
          {
            !isChecked ? 
              <Button
                mode="contained"
                style={[Styles.footerButton, {width:200, marginLeft:-10}]}
                onPress={() => onCheckAvailability()}>
                Check Availability
              </Button>         
            :
            <View>
              {
                process_type === 'edit' 
                ? <Button
                    mode="contained"
                    style={Styles.footerButton}
                    onPress={() => onEditItemCart()}>
                    Save
                  </Button>
                : <Button
                    mode="contained"
                    style={Styles.footerButton}
                    onPress={() => onAddToCart()}>
                    Add To Cart 
                  </Button>
              }
            </View>
          }
        </View>
      </View>
    );
  }
  
  return (
    <View style={Styles.container}>
      <ScrollView>
        <View style={Styles.roomContent}>
          <View style={Styles.imageRoomWrapper}>
            <Image source={{ uri: image }} style={Styles.imageRoom}/>
          </View>
          <View style={Styles.infoRoomWrapper}>
            <Headline>{name}</Headline>
            <Subheading>${price}</Subheading>
            <Caption>{subtitle}</Caption>
          </View>
        </View>
        <View style={Styles.descRoomWrapper}>
          <Divider style={{marginVertical:10}}/>
          <TextInput
            dense={true}
            label='Guest Name'
            value={guestName}
            onChangeText={setGuestName}
            style={Styles.textInputStyle}
            mode='outlined'
            theme={Styles.textInputTheme}
          />
          <View style={{width:200, marginVertical:10}}>
            <Caption>Number of Rooms</Caption>
            <NumberStepper
              dense={true}
              initialValue={numRoom}
              value={numRoom}
              onValueChange={setNumRoom}
              buttonsBackgroundColor='white'
              buttonsFontColor='grey'
              labelBackgroundColor='white'
              labelFontColor='grey'
              buttonsFontSize={30}
              labelFontSize={20}
              buttonsWidth={80}
              borderRadius={4}
            />   
          </View>     
          <TextInput
            dense={true}
            label='Check In Date'
            value={checkInDate}
            onChange={()=>setIsChecked(false)}
            onChangeText={setCheckInDate}
            style={Styles.textInputStyle}
            mode='outlined'
            theme={Styles.textInputTheme}
          />      
          <TextInput
            dense={true}
            label='Check Out Date'
            value={checkOutDate}
            onChange={()=>setIsChecked(false)}
            onChangeText={setcheckOutDate}
            style={Styles.textInputStyle}
            mode='outlined'
            theme={Styles.textInputTheme}
          />                            
          {/* <DateTimePicker
            testID="dateTimePicker"
            timeZoneOffsetInMinutes={0}
            value={checkInDate}
            mode={'date'}
            display="default"
            onChange={onChange}
          />   */}
        </View>
        <View style={Styles.availabilityWrapper}>
          { isChecked && 
            <>
              <Subheading>{rooms.length > 0 ? 'Room Available' : 'Room Not Available'}</Subheading>
              <View style={Styles.availabilityItems}>
                {
                  rooms.length > 0 && rooms.map((r,i)=>
                  <Avatar.Text
                    size={30}
                    label={r.room_number}
                    style={{backgroundColor: r.status==='taken'?'grey':'green', marginRight:5}}
                  />)
                }
              </View>  
            </>
          }
        </View>
      </ScrollView>           
      {footerSection()}
      <Snackbar
        visible={snackConf.visible}
        onDismiss={onResetSnack}
        action={{
          label: 'Dismiss',
          onPress: () => onResetSnack(),
        }}
      >
        {snackConf.message}
      </Snackbar>        
    </View>
  );
}

const mapStateToProps = (state) => {
  return {
      cartItems: state.cartItems
  }
}

const mapDispatchToProps = {
  addToCart: addItemToCart,
  editItem: editCartItem
}

export default connect(mapStateToProps, mapDispatchToProps)(Booking);