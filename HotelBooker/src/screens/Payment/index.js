
import * as React from 'react'
import { View } from 'react-native'
import { Headline, Caption, Subheading, Snackbar, Chip  } from 'react-native-paper'
import StripeButton from '../../components/StripeButton'
import { connect } from 'react-redux';
import moment from 'moment'
import stripe from 'tipsi-stripe'
import { API_ROUTES, API_METHODS } from '../../utils/const'
import { fetchAPI } from '../../utils/API'

import { removeAllCart } from '../../redux/actions'

import Styles from './styles'

stripe.setOptions({
  publishableKey: 'pk_test_S74mcIcWu2vmJeJ2wcVLoGvy00iMrCIiIe'
})

const Payment = (props) => {
  const [snackConf, setSnackConf] = React.useState({visible:false,message:''})
  const [loading, setLoading] = React.useState(false)
  const [token, setToken] = React.useState(null)

  const calculateTotalPrice = (item) => {
    var diff = moment(item.enddate).diff(moment(item.startdate),'days')
    return item.roomtype.price*item.numRoom*diff
  }

  var totalPrice = props.cartItems.reduce((acc, cur) => acc + calculateTotalPrice(cur) , 0)

  const handleCardPayPress = async () => {
    try {
      setLoading(true)
      setToken(null)
      const tempToken = await stripe.paymentRequestWithCardForm({
        // Only iOS support this options
        smsAutofillDisabled: true,
        requiredBillingAddressFields: 'full',
        prefilledInformation: {
          billingAddress: {
            name: 'Gunilla Haugeh',
            line1: 'Canary Place',
            line2: '3',
            city: 'Macon',
            state: 'Georgia',
            country: 'US',
            postalCode: '31217',
            email: 'ghaugeh0@printfriendly.com',
          },
        },
      })
      setLoading(false)
      setToken(tempToken)
      makePayment()
    } catch (error) {
      setLoading(false)
    }
  }

  const makePayment = async () => {
    setLoading(true)
    var payload = {
      stripo: {
        token: token,
        amount: totalPrice,
        currency: 'usd',
      },
      cartdata: props.cartItems.map(ci=>{
        return {
          roomtype: ci.roomtype._id,
          startdate: ci.startdate,
          enddate: ci.enddate,
          guestName: ci.guestName,
          numRoom: ci.numRoom
        }
      })
    }
    var response = await fetchAPI(API_METHODS.POST, API_ROUTES.CHECKOUT, payload)
    if(response.success) {
      submitConfirmation()
    } else {
      setLoading(false)
    }
  }

  const submitConfirmation = async () => {
    var response = await fetchAPI(API_METHODS.POST,API_ROUTES.COMPLETE_PAYMENT, {
      token: token,
      amount: totalPrice,
      currency: 'usd',
    })
    if(response.success) {
      setLoading(false)
      setSnackConf({visible:true,message:'Payment Success'})
      props.removeAllCart()
      props.navigation.navigate('BookingList')   
    } else {
      setLoading(false)
      setSnackConf({visible:true,message:error})
    }
  }  

  const onResetSnack = () => {
    setSnackConf({visible:false,message:''})
  }

  return (
    <View style={Styles.container}>
      <View style={Styles.wrapperContent}>
        <Headline style={{}}>Summary</Headline>
        <Caption>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</Caption>
        <View style={{marginTop:10}}>
          <Subheading>Total Room:</Subheading>
            <View style={{flexDirection:'row'}}> 
            {
              props.cartItems.map(ci=> <Chip icon="home" style={Styles.chips}>{ci.guestName}</Chip>)
            }
            </View>
          <Subheading>Total Cost:</Subheading>
          <Chip icon="coin" style={Styles.chips}>${totalPrice}</Chip>
        </View>      
      </View>
      <View style={[Styles.wrapperContent,{marginTop:230}]}>
        <StripeButton
          text="Enter your card and pay"
          loading={loading}
          onPress={handleCardPayPress}
        />
      </View>      
      <Snackbar
        visible={snackConf.visible}
        onDismiss={onResetSnack}
        action={{
          label: 'Dismiss',
          onPress: () => onResetSnack(),
        }}
      >
        {snackConf.message}
      </Snackbar>        
    </View>
  );
}

const mapStateToProps = (state) => {
  return {
      cartItems: state.cartItems
  }
}
const mapDispatchToProps = {
  removeAllCart: removeAllCart
}


export default connect(mapStateToProps,mapDispatchToProps)(Payment);