import { hp, wp, statusbarHeight } from '../../utils/responsive'
import {StyleSheet} from 'react-native';


export default {
  emptyContainer: {
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center',
    height: hp(80)
  },
  container: {
    flex: 1,
  },
  wrapperContent:{
    paddingHorizontal:20,
    marginTop:5,
    backgroundColor:'white',
    paddingVertical: 20
  },  
  chips:{
    width:100,
    marginVertical: 5,
    marginRight:2
  },
  roomContent:{
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'flex-end',
    marginTop: 10,
    backgroundColor: 'white',
  },
  imageRoomWrapper:{
    width: wp(40),
  },
  imageRoom:{
    width:120,
    height:140,
  },
  infoRoomWrapper:{
    width: wp(60),
    paddingRight:20,
    paddingVertical: 10
  },
  descRoomWrapper:{
    paddingHorizontal: 20,
  },
  availabilityWrapper:{
    paddingHorizontal: 20,
    marginVertical: 40,
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center' 
  },  
  availabilityItems:{
    marginTop:30,
    flex: 1, 
    flexDirection: 'row'
  },
  footerContainer: {
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  totalCostWrapper: {
    width: wp(40),
    paddingLeft: 15,
    paddingRight: 10,
    paddingVertical: 10,
    justifyContent: 'center',
  },
  sectionLabel: {
    fontSize: 13,
    color: '#666666',
    fontFamily: 'Roboto',
    alignSelf: 'center'
  },
  totalText: {
    fontSize: 30,
    color: '#1a963d',
    fontFamily: 'Roboto-Medium',
    alignSelf: 'center'
  },
  footerButtonWrapper: {
    width: wp(60),
    paddingRight: 30,
    paddingLeft: 10,
    justifyContent: 'center',
  },
  footerButton: {
    backgroundColor: '#003f3a',
  },
  textInputTheme: {
    colors: {
      primary: '#47828f',
      underlineColor: '#47828f',
    },
  }
}