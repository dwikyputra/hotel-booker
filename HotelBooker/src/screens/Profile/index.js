
import * as React from 'react';
import { View } from 'react-native';
import { Button, Headline } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import { NavigationActions } from '@react-navigation/compat'
import { StackActions } from '@react-navigation/native';

export default ProfileScreen = ({ navigation }) => {
  const onSignOut = () => {
    navigation.dispatch(StackActions.popToTop());
    AsyncStorage.removeItem("token"); 
    AsyncStorage.removeItem("userId"); 
  } 
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Headline>Profile</Headline>
      <Button mode="outlined" onPress={() => onSignOut()}>
        SignOut
      </Button>                       
    </View>
  );
}