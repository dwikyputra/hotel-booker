
import * as React from 'react';
import { View, Text, Image } from 'react-native';
import { Button, Headline, Caption, Paragraph, Divider } from 'react-native-paper';
import Styles from './styles'

export default RoomTypeScreen = ({ route, navigation }) => {
  const { _id, name, subtitle, image, description, price } = route.params

  const footerSection = (price) => (
    <View style={Styles.footerContainer}>
      <View style={Styles.totalCostWrapper}>
        <Text style={Styles.sectionLabel}>Price</Text>
        <Text style={Styles.totalText}>${price}</Text>
      </View>
      <View style={Styles.footerButtonWrapper}>
        <Button
          mode="contained"
          style={Styles.footerButton}
          onPress={() => navigation.navigate('Booking', {...route.params})}>
          Book Now
        </Button>
      </View>
    </View>
  );
  
  return (
    <View style={Styles.container}>
      <Image source={{ uri: image }} style={{width:'100%', height:250}}/>
      <View style={{paddingHorizontal:20, marginTop: 20}}>
        <Headline>{name}</Headline>
        <Caption>{subtitle}</Caption>
        <Divider style={{marginVertical:10}}/>
        <Paragraph style={{fontSize:15}}>{description}</Paragraph>
      </View>
      {footerSection(price)}
    </View>
  );
}