import { hp, wp, statusbarHeight } from '../../utils/responsive'
import {StyleSheet} from 'react-native';


export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'
  },
  footerContainer: {
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    height: 40,
    flex: 1,
    justifyContent: 'flex-end',
    marginTop: 70
  },
  totalCostWrapper: {
    width: '40%',
    paddingHorizontal: 15,
    paddingVertical: 10,
    justifyContent: 'center',
  },
  sectionLabel: {
    fontSize: 13,
    color: '#666666',
    fontFamily: 'Roboto',
    alignSelf: 'center'
  },
  totalText: {
    fontSize: 30,
    color: '#1a963d',
    fontFamily: 'Roboto-Medium',
    marginTop: -2,
    alignSelf: 'center'
  },
  footerButtonWrapper: {
    width: '60%',
    padding: 10,
    justifyContent: 'center',
  },
  footerButton: {
    backgroundColor: '#003f3a',
  },
})