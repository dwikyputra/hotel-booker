import * as React from 'react';
import { View, Image } from 'react-native';
import { Button, TextInput, Snackbar } from 'react-native-paper';
import { connect } from 'react-redux';
import Styles from './styles'
import { API_ROUTES, API_METHODS } from '../../utils/const'
import { fetchAPI } from '../../utils/API'
import AsyncStorage from '@react-native-community/async-storage';


const AuthScreen = ({ navigation }) => {
  const [email, setEmail] = React.useState('memberone@hotelbooker.id');
  const [password, setPassword] = React.useState('password123');
  const [snackConf, setSnackConf] = React.useState({visible:false,message:''});

  const onLoginPressed = async () => {

    var response = await fetchAPI(API_METHODS.POST, API_ROUTES.SIGNIN, {
      email: email,
      password: password,
    })
    if(response.success) {
      AsyncStorage.setItem("token", response.data.token); 
      AsyncStorage.setItem("userId", response.data.user._id); 
      navigation.navigate('Main')      
    } else {
      setSnackConf({visible:true,message:response.message})
    }
  }

  // React.useEffect(() => {
  //   setSnackConf({visible:true,message:'props updated'})
  //   console.log(props.cartItems)
  // }, [props]);

  const onResetSnack = () => {
    setSnackConf({visible:false,message:''})
  }

  return (
    <>
      <View style={Styles.container}>
        <View style={Styles.backgroundWrapper}>
          <Image source={require('../../assets/images/backgroundmount.png')} style={Styles.backgroundImage} />
        </View>
        <View style={Styles.formWrapper}>
          <TextInput
            dense={true}
            label='Email'
            value={email}
            onChangeText={setEmail}
            style={Styles.textInputStyle}
            mode='outlined'
            theme={Styles.textInputTheme}
          />
          <TextInput
          dense={true}
            label='Password'
            value={password}
            onChangeText={setPassword}
            style={Styles.textInputStyle}
            mode='outlined'
            theme={Styles.textInputTheme}
            secureTextEntry={true}
          />        
          <Button
            mode="contained"
            onPress={()=>onLoginPressed()} 
            contentStyle={{height:40}}
            style={{marginTop:20,backgroundColor:'#003f3a'}}
            // loading={props.loading}
          >
            Login
          </Button>    
        </View>  
      </View>
      <Snackbar
        visible={snackConf.visible}
        onDismiss={onResetSnack}
        action={{
          label: 'Dismiss',
          onPress: () => onResetSnack(),
        }}
      >
        {snackConf.message}
      </Snackbar>      
    </>
  );
}


export default connect()(AuthScreen);