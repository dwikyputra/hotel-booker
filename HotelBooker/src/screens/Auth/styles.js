import { StyleSheet } from 'react-native';
import { hp, wp } from '../../utils/responsive'

export default {
  container: { 
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center' 
  },
  backgroundWrapper: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: '100%',
    width: '100%'
  },
  backgroundImage: {
    flex: 1,
    height: null,
    width: null
  },
  formWrapper:{
    marginTop: hp(10),
    height: hp(40),
    width: wp(80),
  },
  textInputStyle: {
    color: '#000000',
    backgroundColor: 'white',
    textAlignVertical: 'top',
    fontFamily: 'Roboto',
    fontSize: 14,
  },
  textInputTheme: {
    colors: {
      primary: '#47828f',
      underlineColor: '#47828f',
    },
  }
}