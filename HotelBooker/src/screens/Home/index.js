
import * as React from 'react';
import { SafeAreaView, View, TouchableOpacity, FlatList, Text } from 'react-native';
import { Card, Title, Paragraph, Snackbar } from 'react-native-paper';
import Styles from './styles'
import { API_ROUTES, API_METHODS } from '../../utils/const'
import { fetchAPI } from '../../utils/API'

export default HomeScreen = ({ route, navigation }) => {
  const [data, setData] = React.useState([]);
  const [snackConf, setSnackConf] = React.useState({visible:false,message:''});

  React.useEffect(() => {
    const fetchData = async () => {
      var response = await fetchAPI(API_METHODS.GET, API_ROUTES.ROOM_TYPES)
      if(response.success) {
         setData(response.data) 
      } else {
        setSnackConf({visible:true,message:response.message})
      }
    }

    if(!route.params==false) {
      setSnackConf({visible:true,message:response.message})
    } else fetchData()
  }, []);

  const onResetSnack = () => {
    setSnackConf({visible:false,message:''})
  }

  return (
    <View style={Styles.container}>
      <SafeAreaView style={Styles.contentWrapper}>
        <FlatList
          style={Styles.listStyle}
          contentContainerStyle={Styles.listContainerStyle}
          data={data}
          renderItem={({ item }) => (
            <>
              <View style={Styles.rectangle}>
                <Text style={{fontSize: 12, color: '#FFFFFF', fontFamily: 'Roboto'}}>ONLY</Text>
                <Text style={{fontSize: 14, color: '#FFFFFF', fontFamily: 'Roboto-Medium'}}>
                  $ {item.price}
                </Text>
              </View>
              <TouchableOpacity activeOpacity={0.80} style={{paddingHorizontal:10}} onPress={()=>navigation.navigate('RoomType',{...item})}>
                <Card style={Styles.cardStyle}>
                  <Card.Cover source={{ uri: item.image }} />
                  <Card.Content>
                    <Title style={{paddingTop:10}}>{item.name}</Title>
                    <Paragraph>{item.subtitle}</Paragraph>
                  </Card.Content>              
                </Card>   
              </TouchableOpacity>  
            </>                  
          )}
          keyExtractor={item => item._id}
        />
      </SafeAreaView>
      <Snackbar
        visible={snackConf.visible}
        onDismiss={onResetSnack}
        action={{
          label: 'Dismiss',
          onPress: () => onResetSnack(),
        }}
      >
        {snackConf.message}
      </Snackbar>
    </View>
  );
}