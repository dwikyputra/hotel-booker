import { hp, wp, statusbarHeight } from '../../utils/responsive'
import {StyleSheet} from 'react-native';


export default StyleSheet.create({
  container: { 
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center' 
  },
  backgroundWrapper: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: '100%',
    width: '100%'
  },
  backgroundImage: {
    flex: 1,
    height: null,
    width: null
  },
  contentWrapper: {
    paddingTop: statusbarHeight,
    marginTop: hp(2),
    marginBottom: hp(5),
    height: hp(88),
    width: wp(100)
  },
  listStyle: {
    width: wp(100),    
  },
  listContainerStyle:{
    alignItems: 'center', 
    justifyContent: 'center'    
  },
  cardStyle:{
    width: wp(90),
    marginVertical: 10,
    zIndex: 1
  },
  rectangle: {
    width: 60,
    height: 60,
    backgroundColor: '#27AE60',
    position: 'absolute',
    zIndex: 5,
    alignSelf: 'flex-end',
    borderTopLeftRadius: 7,
    borderTopRightRadius: 7,
    borderRadius: 7,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },  
})