
import * as React from 'react'
import { View, Text, Image, ScrollView, TouchableOpacity } from 'react-native'
import { Button, Headline, Caption, Subheading, Snackbar  } from 'react-native-paper'
import { connect } from 'react-redux';
import moment from 'moment'
import Icon from 'react-native-vector-icons/AntDesign' 
import { API_ROUTES, API_METHODS } from '../../utils/const'

import { removeItemFromCart, removeAllCart } from '../../redux/actions'

import Styles from './styles'

const Transaction = (props) => {
  const [data, setData] = React.useState([]);
  const [snackConf, setSnackConf] = React.useState({visible:false,message:''})

  React.useLayoutEffect(() => {
    props.navigation.setOptions({
      headerRight: () => (
        deleteAllCartIcon()
      ),
    });
  }, [props.navigation]);

  React.useEffect(() => {
    const fetchData = async () => {
      var response = await fetchAPI(API_METHODS.GET, API_ROUTES.TRANSACTIONS)
      if(response.success) {
         setData(response.data) 
      } else {
        setSnackConf({visible:true,message:response.message})
      }
    }

    if(!props.route.params==false) {
      setSnackConf({visible:true,message:response.message})
    } else fetchData()
  }, []);  

  const deleteAllCartIcon = () => (
    <TouchableOpacity activeOpacity={0.80} onPress={()=>props.removeAllCart()} style={{marginRight:10}}>
      <Icon name="delete" size={30}  color='#a80000'/>
    </TouchableOpacity>
  ) 

  const onResetSnack = () => {
    setSnackConf({visible:false,message:''})
  }

  const calculateTotalPrice = (item) => {
    var diff = moment(item.enddate).diff(moment(item.startdate),'days')
    return item.roomtype.price*item.numRoom*diff
  }

    const cartItem = (item, i) => {
      var diff = moment(item.enddate).diff(moment(item.startdate),'days')
      var bookingData = { process_type:'edit', cartId:item.cartId, ...item.roomtype, numroom:item.numRoom, guestname:item.guestName, startdate:item.startdate, enddate:item.enddate }
      return (
        <TouchableOpacity activeOpacity={0.80} onPress={()=>props.navigation.navigate('Booking',bookingData)}>
          <View style={Styles.roomContent} key={i}>
            <View style={Styles.imageRoomWrapper}>
              <Image source={{ uri: item.roomtype.image }} style={Styles.imageRoom}/>
            </View>
            <View style={Styles.infoRoomWrapper}>
              <Headline>{item.roomtype.name}</Headline>
              <Caption>{item.roomtype.subtitle}</Caption>
              <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                <View style={{flexDirection:'row', justifyContent:'space-between', width:'75%'}}>
                  <Subheading>${calculateTotalPrice(item)}</Subheading>
                  <Subheading>{item.numRoom}r</Subheading>
                  <Subheading>{diff}d</Subheading>
                  <Subheading>{item.guestName}</Subheading>
                </View>     
                <TouchableOpacity activeOpacity={0.80} onPress={()=>onDeletePressed(item.cartId)}>      
                  <Icon name="delete" size={30} color='#a80000'/>
                </TouchableOpacity>   
              </View>
            </View>
          </View>
        </TouchableOpacity>
      )
    }
  
  return (
    <View style={Styles.container}>
      <ScrollView>
        {
          props.cartItems.length > 0 
          ? props.cartItems.map((item, i) => cartItem(item))
          : <View style={Styles.emptyContainer}><Text>Empty Cart</Text></View>
        }

      </ScrollView>           
      <Snackbar
        visible={snackConf.visible}
        onDismiss={onResetSnack}
        action={{
          label: 'Dismiss',
          onPress: () => onResetSnack(),
        }}
      >
        {snackConf.message}
      </Snackbar>        
    </View>
  );
}

const mapStateToProps = (state) => {
  return {
      cartItems: state.cartItems
  }
}

const mapDispatchToProps = {
  removeFromCart: removeItemFromCart,
  removeAllCart: removeAllCart
}

export default connect(mapStateToProps, mapDispatchToProps)(Transaction);