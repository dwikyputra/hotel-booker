import React from "react";
import {
    View,
    Text,
    StyleSheet,
    Platform,
    TouchableOpacity
} from "react-native";

import { withNavigation } from '@react-navigation/compat';

import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/AntDesign'

const ShoppingCartIcon = (props) => (
    <TouchableOpacity activeOpacity={0.80} onPress={()=>props.navigation.navigate('Cart')}>
        <View style={[{ padding: 5 }, Platform.OS == 'android' ? styles.iconContainer : null]}>
            <View style={{
                position:'absolute', height:26, width:26, borderRadius:15, backgroundColor:'#e7ce00d4', right:15, bottom:15, alignItems:'center', justifyContent:'center', zIndex:2000,
            }}>
                <Text style={{ color: 'white', fontWeight: 'bold' }}>{props.cartItems.length}</Text>
            </View>
            <Icon name="shoppingcart" size={30} />
        </View>
    </TouchableOpacity>
)

const mapStateToProps = (state) => {
    return {
        cartItems: state.cartItems
    }
}

export default connect(mapStateToProps)(withNavigation(ShoppingCartIcon));

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    iconContainer: {
        paddingLeft: 20, paddingTop: 10, marginRight: 5
    }
});