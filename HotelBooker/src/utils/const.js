
const isDevelopment = false
export const API_URL = isDevelopment ? 'http://192.168.1.102:4000/api' : 'http://206.189.189.243:4000/api'
export const API_ROUTES = {
  SIGNIN: '/signin',
  SIGNOUT: '/signout',
  HOME: '/home',
  ROOMS: '/rooms',
  ROOM_TYPES: '/room_types',
  BOOKINGS: '/mybookings',
  TRANSACTIONS: '/transactions',
  ROOM_AVAILABLE: '/roomschedules/search',
  CHECKOUT: '/checkout',
  COMPLETE_PAYMENT: '/complete_payment'
}

export const API_METHODS = {
  POST: 'POST',
  GET: 'GET',
  PUT: 'PUT',
  DELETE: 'DELETE'
}
