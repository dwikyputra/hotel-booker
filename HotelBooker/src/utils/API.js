import AsyncStorage from '@react-native-community/async-storage';

import {API_URL, API_METHODS} from '../utils/const'

export const fetchAPI = async (method, routes, data={}) => {
  var token = await AsyncStorage.getItem('token');
  var basicheader = { "Content-type": "application/json; charset=UTF-8" }
  var config = {
    method: method,
    headers: {...basicheader, key:token }
  }
  if(method===API_METHODS.POST || method===API_METHODS.PUT) config.body = JSON.stringify(data)
  try {
    let res = await fetch(API_URL+routes, config);
    var response = await res.json();
  } catch (e) {
    console.log(`😱 Fetch request failed: ${e}`);
    return {success:false, message:e }
  }
  if (response.status === 200) {
    return {success:true, data:response.data }
  } else {
    return {success:false, message:response.message }
  }
}