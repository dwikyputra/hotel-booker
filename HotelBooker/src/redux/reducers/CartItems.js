import {TYPES} from "../const"

const cartItems = (state=[], action) => {
    switch(action.type){
        case TYPES.ADD_TO_CART:
            return [
                ...state,
                {
                    roomtype: action.payload.roomtype,
                    startdate: action.payload.startdate,
                    enddate: action.payload.enddate,
                    guestName: action.payload.guestName,
                    numRoom: action.payload.numRoom,                    
                    cartId: action.id
                }
            ]

        case TYPES.UPDATE_CART_ITEM:
            return [ 
                ...state.filter(cartItem=>cartItem.cartId !== action.cartId),
                {
                    roomtype: action.payload.roomtype,
                    startdate: action.payload.startdate,
                    enddate: action.payload.enddate,
                    guestName: action.payload.guestName,
                    numRoom: action.payload.numRoom,                    
                    cartId: action.cartId                    
                }
            ]

        case TYPES.REMOVE_FROM_CART:
            return state.filter(cartItem=>cartItem.cartId !== action.cartId)         
            
        case TYPES.REMOVE_ALL_CART:
            return []                 
    }
    return state
}

export default cartItems