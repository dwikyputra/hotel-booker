
import {TYPES} from "../const"

const categories = (state = [], action) => {
    switch(action.type){
        case TYPES.FETCH_ROOM_TYPES_SUCCESS:
            return action.payload
        default:
            return state
    }
}

export default categories
