import {combineReducers} from 'redux'
import roomTypes from './RoomTypes'
import rooms from './Rooms'
import cartItems from './CartItems'
import loading from './Loading'
import memberAuth from './MemberAuth'

export default combineReducers({
    roomTypes,
    rooms,
    cartItems,
    loading,
    memberAuth
})