import {TYPES} from "../const"

const loading = (state=false, action) => {
    switch(action.type){
        case TYPES.LOADING_STARTED:
            return true
        case TYPES.LOADING_FINISHED:
            return false
    }
    return state
}

export default loading