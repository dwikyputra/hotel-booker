import {TYPES} from "../const"

const memberAuth = (state={name:'bambang'}, action) => {
  switch(action.type){
    case TYPES.FETCH_SIGNIN_SUCCESS:
        return {...action.payload.user, token:action.payload.token}
    case TYPES.FETCH_SIGNIN_FAILED:
        return state       
    default:
        return state
  }
}

export default memberAuth