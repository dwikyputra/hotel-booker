import {TYPES} from "../const"

const rooms = (state = [], action) => {
    switch(action.type){
        case TYPES.FETCH_ROOMS_SUCCESS:
            return action.payload
        default:
            return state
    }
}

export default rooms
