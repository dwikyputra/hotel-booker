import axios from 'axios'
import { TYPES, API_URL, API_ROUTES } from "../const"


export const fetchLogin = (data) => {
    return (dispacth, getState) => {
        dispacth({type: TYPES.LOADING_STARTED})
        axios.post(API_URL+API_ROUTES.SIGNIN, data)
        .then( function(response){
            dispacth({type: TYPES.LOADING_FINISHED})
            if(response.status==200) dispacth({type: TYPES.FETCH_SIGNIN_SUCCESS, payload:response.data.data})
            else dispacth({type: TYPES.FETCH_SIGNIN_FAILED})
        })
        .catch( function(error){
            dispacth({type: TYPES.LOADING_FINISHED})
            dispacth({type: TYPES.FETCH_SIGNIN_FAILED})
        })
    }
}


let nextCartId = 0
export const addItemToCart = (data) => ({ 
    type: TYPES.ADD_TO_CART, 
    payload: data,
    id: nextCartId++
})

export const removeItemFromCart = (id) => ({ 
    type: TYPES.REMOVE_FROM_CART,
    cartId: id
})

export const editCartItem = (id, data) => ({ 
    type: TYPES.UPDATE_CART_ITEM,
    cartId: id,
    payload: data
})

export const removeAllCart = () => ({ 
    type: TYPES.REMOVE_ALL_CART
})

export const fetchRoomType = (url) => {
    return (dispacth, getState) => {
        dispacth({type: TYPES.LOADING_STARTED})
        axios.get(url)
        .then( function(response){
            dispacth({type: TYPES.LOADING_FINISHED})
            dispacth({type: TYPES.FETCH_ROOM_TYPES_SUCCESS, payload:response.data.data})
        })
        .catch( function(error){
            dispacth({type: TYPES.LOADING_FINISHED})
        })
    }
}

export const fetchRoom = (url) => {
    return (dispacth, getState) => {
        dispacth({type: TYPES.LOADING_STARTED})
        axios.get(url)
        .then( function(response){
            dispacth({type: TYPES.LOADING_FINISHED})
            dispacth({type: TYPES.FETCH_ROOMS_SUCCESS, payload:response.data.data})
        })
        .catch( function(error){
            dispacth({type: TYPES.LOADING_FINISHED})
        })
    }
}
