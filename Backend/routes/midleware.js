Session = require('../models/session.model');
User = require('../models/user.model');
Mail = require('../models/mail.model');
Room = require('../models/room.model');
RoomType = require('../models/room_type.model');
Booking = require('../models/booking.model');
Transaction = require('../models/transaction.model');


function HasRole(role) {
  return function(req, res, next) {
    if (!role.includes(res.user.role))  return res.json({ status:400, message: "You don't have enough previlege" })
    else next()
  }
}
exports.hasRoleAdmin = HasRole(['admin']);
exports.hasRoleMember = HasRole(['member']);


exports.authenticate = async (req, res, next) => {
  var passwordhash = crypto.createHash('md5').update(req.body.password).digest('hex');
  try {
    user = await User.findOne({'email': req.body.email,'password': passwordhash})
    if (user == null) {
      return res.json({ status:400, message: 'Cant find user, please check your email or password'})
    }
  } catch(err){
    return res.json({ status:400, message: err.message })
  }
  res.user = user
  next()
}

exports.validate = async (req, res, next) => {
  console.log(req.route.path)
  try {
    session = await Session.findOne({token: req.headers.key}).populate("user")
    if (session == null) {
      return res.json({ status:400, message: 'Token not found or expired'})
    }
  } catch(err){
    return res.json({ status:400, message: err.message })
  }
  res.user = session.user
  next()
};

exports.getUser = async (req, res, next) => {
  try {
    user = await User.findById(req.params.id)
    if (user == null) {
      return res.json({ status:400, message: 'User not found'})
    }
  } catch(err){
    return res.json({ status:400, message: err.message })
  }
  res.user = user
  next()
}

exports.getMail = async (req, res, next) => {
  try {
    mail = await Mail.findById(req.params.id).populate('sender recipient')
    if (mail == null) {
      return res.json({ status:400, message: 'Mail not found'})
    }
  } catch(err){
    return res.json({ status:400, message: err.message })
  }
  res.mail = mail
  next()
}

exports.getRoomType = async (req, res, next) => {
  try {
    room_type = await RoomType.findById(req.params.id)
    if (room_type == null) {
      return res.json({ status:400, message: 'Room type not found'})
    }
  } catch(err){
    return res.json({ status:400, message: err.message })
  }
  res.room_type = room_type
  next()
}

exports.getRoom = async (req, res, next) => {
  try {
    room = await Room.findById(req.params.id).populate('room_type')
    if (room == null) {
      return res.json({ status:400, message: 'Room type not found'})
    }
  } catch(err){
    return res.json({ status:400, message: err.message })
  }
  res.room = room
  next()
}


exports.getBooking = async (req, res, next) => {
  try {
    booking = await Booking.findById(req.params.id).populate({ path: 'member room', populate: { path: 'room_type' }})
    if (booking == null) {
      return res.json({ status:400, message: 'Booking not found'})
    }
  } catch(err){
    return res.json({ status:400, message: err.message })
  }
  res.booking = booking
  next()
}


exports.getTransaction = async (req, res, next) => {
  try {
    transaction = await Transaction.findById(req.params.id).populate('bookings member')
    if (transaction == null) {
      return res.json({ status:400, message: 'Transaction not found'})
    }
  } catch(err){
    return res.json({ status:400, message: err.message })
  }
  res.transaction = transaction
  next()
}
