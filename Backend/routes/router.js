let router = require('express').Router();
var authController = require('../controllers/auth.controller');
var userController = require('../controllers/user.controller');
var mailController = require('../controllers/mail.controller');
var roomController = require('../controllers/room.controller');
var roomTypeController = require('../controllers/room_type.controller');
var roomScheduleController = require('../controllers/room_schedule.controller');
var bookingController = require('../controllers/booking.controller');
var transactionController = require('../controllers/transaction.controller');

const { validate, authenticate, hasRoleAdmin, getUser, getMail, getRoom, getRoomType, getBooking, getTransaction } = require('./midleware')

// Auth routes
router.route('/testfirebase')
  .post(authController.testFCM)

// Auth routes
router.route('/signin')
  .post(authenticate, authController.signin)

// User routes
router.route('/users')
  .get([validate, hasRoleAdmin], userController.all)
  .post(validate, userController.create);
router.route('/users/:id')
  .get([validate, getUser], userController.read)
  .put([validate, getUser], userController.update)
  .delete([validate, getUser], userController.delete);

// Mail routes
router.route('/mails')
  .get(validate, mailController.all)
  .post(validate, mailController.create);
router.route('/mails/:id')
  .get([validate, getMail], mailController.read)
  .put([validate, getMail], mailController.update)
  .delete([validate, getMail], mailController.delete);

// Room routes
router.route('/rooms')
  .get(validate, roomController.all)
  .post(validate, roomController.create);
router.route('/rooms/:id')
  .get([validate, getRoom], roomController.read)
  .put([validate, getRoom], roomController.update)
  .delete([validate, getRoom], roomController.delete);

// Room type routes
router.route('/room_types')
  .get(validate, roomTypeController.all)
  .post(validate, roomTypeController.create);
router.route('/room_types/:id')
  .get([validate, getRoomType], roomTypeController.read)
  .put([validate, getRoomType], roomTypeController.update)
  .delete([validate, getRoomType], roomTypeController.delete);

// Room Schedule routes
router.route('/roomschedules')
  .get(validate, roomScheduleController.all)
router.route('/roomschedules/search')
  .post(validate, roomScheduleController.scheduleByDateAndType)
router.route('/roomschedules/:id')
  .delete(validate, roomScheduleController.delete)

// Booking routes
router.route('/mybookings')
  .get(validate, bookingController.allByUser)
router.route('/bookings')
  .get([validate, hasRoleAdmin], bookingController.all)
  .post([validate, bookingController.validate('create')], bookingController.create);
router.route('/bookings/:id')
  .get([validate, getBooking], bookingController.read)
  .put([validate, getBooking], bookingController.update)
  .delete([validate, getBooking], bookingController.delete);

// Transaction routes
router.route('/transactions')
  .get(validate, transactionController.all)
router.route('/confirmation/:id/:val')
  .get([validate, getTransaction], transactionController.confirmation)  
router.route('/transactions/:id')
  .get([validate, getTransaction], transactionController.read)
  .put([validate, getTransaction], transactionController.update)
  .delete([validate, getTransaction], transactionController.delete)
router.route('/checkout')
  .post(validate, transactionController.checkout);  
router.route('/complete_payment')
  .post(transactionController.completePaymentWithStripe)
  

module.exports = router;