import React from "react";
import { UserOutlined, ProfileOutlined, MailOutlined, BarChartOutlined, CarryOutOutlined } from '@ant-design/icons';

import Menus from "./pages/Menus";
import LoginPage from "./pages/Login/LoginPage";
import NotFoundPage from "./pages/Common/NotFoundPage";

import PublicLayout from "./shared/layout/PublicLayout";
import AuthLayout from "./shared/layout/AuthLayout";

import DashboardView from "./pages/Dashboard";
import UserView from "./pages/Users";
import MailView from "./pages/Mails";
import SingleMailBox from "./pages/Mails/SingleMailBox";
import BookingView from "./pages/Bookings";
import TransactionView from "./pages/Transactions";


export default {
  // Outer Menu 
  menu: [
    {
      exact: true,
      path: "/login",
      component: LoginPage,
      layout: PublicLayout
    },
    {
      exact: false,
      path: "/notfound",
      component: NotFoundPage,
      layout: PublicLayout
    },
    {
      exact: false,
      path: "",
      component: Menus,
      layout: AuthLayout
    }    
  ],  

  // Nested Dashboard Menu
  dashboardmenu:[ 
    {
      path: "/",
      text: "Dashboard",
      icon: <ProfileOutlined/>,
      component: DashboardView
    },    
    {
      path: "/user",
      text: "User",
      icon: <UserOutlined/>,
      component: UserView
    },
    {
      path: "/booking",
      text: "Booking",
      icon: <CarryOutOutlined/>,
      component: BookingView
    },
    {
      path: "/transaction",
      text: "Transaction",
      icon: <BarChartOutlined/>,
      component: TransactionView
    },
    {
      path: "/mail",
      text: "Mailbox",
      icon: <MailOutlined/>,
      component: MailView,
      extraprops: { name: "Hello world" }
    },            
  ],

  // Other menus
  othermenu:[ 
    {
      path: "/mail/:id",
      component: SingleMailBox
    },               
  ]
};
