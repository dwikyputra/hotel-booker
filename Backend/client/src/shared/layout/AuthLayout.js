import React from "react";
import { message, Row, Layout, Button, Dropdown, Menu } from "antd";
import routes from "../../routes";
import { DownOutlined, LogoutOutlined } from '@ant-design/icons';

import logo from '../assets/images/logo.svg';

const { Header, Sider, Content } = Layout;

class AuthLayout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: false
    }
  }

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  componentDidMount() {
    if (!localStorage.getItem("token")) {
      // User is not logged in. Redirect back to login
      this.props.history.push('/login');
      message.warning("Please login first");
      return;
    }
    // Fetch data for logged in user using token
  }

  onLogout = () => {
    // Remove token & other stored data
    localStorage.clear();
    this.props.history.push('/login');
  };

  render() {
    return (
      <Layout>
        <Sider trigger={null} collapsible collapsed={this.state.collapsed} style={{background:'#fff'}} width={256}>
          <div className="app-logo"><img src={logo}  alt="logo" /></div>
          <Menu mode="inline" defaultSelectedKeys={['1']} >        
            {
              routes.dashboardmenu.map((menu,i)=>{
                return (
                  <Menu.Item key={i+1} onClick={()=>this.props.history.push(menu.path)}>
                    {menu.icon}
                    <span>{menu.text}</span>
                  </Menu.Item>
                )
              })
            }
          </Menu>
        </Sider>
        <Header
          style={{
            position: "absolute",
            zIndex: 1,
            width: "100%",
            backgroundColor: "#096dd9"
          }}
        >
          <Row
            type="flex"
            align="middle"
            justify="end"
            style={{ height: "100%" }}
          >
            {/* Dropdown with option to logout */}
            <Dropdown
              overlay={
                <Menu>
                  <Menu.Item key="1" onClick={this.onLogout}>
                    <LogoutOutlined />
                    Logout
                  </Menu.Item>
                </Menu>
              }
              trigger={["click"]}
            >
              <Button ghost className="primary-btn">
                Administrator <DownOutlined />
              </Button>
            </Dropdown>
          </Row>
        </Header>        
        <Layout className="site-layout"> 
          <Content style={{ padding: "0 50px", marginTop: 64, minHeight: "92vh" }}>
            {this.props.children}
          </Content>
        </Layout>
      </Layout>
    );
  }
}

export default AuthLayout;
