export const formatCurrency = (value) => {
  value = typeof value != 'undefined' ? value : 0
  return value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
}