import moment from 'moment'

export const isDevelopment = false

export const exportTransactionFileName = 'exported-transaction-'+moment().format()+'.csv'