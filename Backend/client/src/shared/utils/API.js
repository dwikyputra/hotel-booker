import { isDevelopment } from './Setting'
export const APIURL = isDevelopment? "http://localhost:4000/api/": "http://206.189.189.243:4000/api/"

export const fetchGet = async (endpoint, headers) => {
  var basicheader = { "Content-type": "application/json; charset=UTF-8" }
  const requestOptions = {
    method: 'GET',
    headers: {...basicheader, ...headers } ,
  };  
  try {
    const response = await fetch(APIURL+endpoint, requestOptions);
    var resdata = await response.json();
  } catch (e) {
    console.log(`😱 Fetch request failed: ${e}`);
    return {success:false, message:e }
  }
  if (resdata.status === 200) {
    return {success:true, data:resdata }
  } else {
    return {success:false, data:resdata }
  }
}

export const fetchDelete = async (endpoint, headers) => {
  var basicheader = { "Content-type": "application/json; charset=UTF-8" }
  const requestOptions = {
    method: 'DELETE',
    headers: {...basicheader, ...headers } ,
  };   
  try {
    const response = await fetch(APIURL+endpoint, requestOptions);
    var resdata = await response.json();
  } catch (e) {
    console.log(`😱 Fetch request failed: ${e}`);
    return {success:false, message:e }
  }
  if (resdata.status === 200) {
    return {success:true, data:resdata }
  } else {
    return {success:false, data:resdata }
  }
}

export const fetchPost = async (endpoint, headers, data) => {
  var basicheader = { "Content-type": "application/json; charset=UTF-8" }
  const requestOptions = {
    method: 'POST',
    headers: {...basicheader, ...headers } ,
    body: JSON.stringify(data)
  };   
  try {
    const response = await fetch(APIURL+endpoint, requestOptions);
    var resdata = await response.json();
    console.log(resdata)
  } catch (e) {
    console.log(`😱 Fetch request failed: ${e}`);
    return {success:false, message:e }
  }

  if (resdata.status === 200) {
    console.log('masuk')
    return {success:true, data:resdata }
  } else {
    console.log('err')
    return {success:false, data:resdata }
  }
}

export const fetchPut = async (endpoint, headers, data) => {
  var basicheader = { "Content-type": "application/json; charset=UTF-8" }
  const requestOptions = {
    method: 'PUT',
    headers: {...basicheader, ...headers } ,
    body: JSON.stringify(data)
  };   
  try {
    const response = await fetch(APIURL+endpoint, requestOptions);
    var resdata = await response.json();
  } catch (e) {
    console.log(`😱 Fetch request failed: ${e}`);
    return {success:false, message:e }
  }
  if (resdata.status === 200) {
    return {success:true, data:resdata }
  } else {
    return {success:false, data:resdata }
  }
}