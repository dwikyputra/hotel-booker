import React, { useState, useEffect } from "react";
import { message, Table, Popconfirm, Button } from "antd";
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { fetchGet, fetchDelete } from '../../shared/utils/API'


const UserView = props => {
  const [data, setData] = useState([]);

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      sorter: (a, b) => {
        if (a.name < b.name) return -1
        if (a.name > b.name) return 1;
        return 0;
      },
      render: text => <a href="/">{text}</a>,
    },
    {
      title: 'Gender',
      dataIndex: 'gender',
      key: 'gender',
      defaultSortOrder: 'descend',
      sorter: (a, b) => {
        if (a.gender < b.gender) return -1
        if (a.gender > b.gender) return 1;
        return 0;
      },
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
      sorter: (a, b) => {
        if (a.email < b.email) return -1
        if (a.email > b.email) return 1;
        return 0;
      },
    },
    {
      title: 'Phone',
      key: 'phone',
      dataIndex: 'phone',
      sorter: (a, b) => a.phone - b.phone,
    },
    {
      title: 'Action',
      key: 'action',
      render: (text, record) => (
        <span>
          <Button style={{ marginRight: 16 }} type="primary" shape="circle" icon={<EditOutlined />} />
          <Popconfirm title="Sure to delete?" onConfirm={() => handleDelete(record._id)}>
            <Button type="primary" shape="circle" danger icon={<DeleteOutlined />} disabled={record.role==='admin'}/>
          </Popconfirm>
        </span>
      ),    
    },
  ];

  const handleDelete = async (id) => {
    var token = localStorage.getItem("token")
    var res = await fetchDelete('users/'+id, {key:token})
    if(!res.success) message.warning("Delete user data failed");
    else {
      const dataSource = [...data];
      setData(dataSource.filter(item => item._id !== id),)   
    }
  };

  useEffect( () => {
    const fetchData = async () => {
      var token = localStorage.getItem("token")
      var res = await fetchGet('users', {key:token})
      if(!res.success) message.warning("Get user data failed");
      else {
        setData(res.data.data)
      }
    };
    fetchData();
  },[]);

  return (
    <div style={{ marginTop: 120 }}>
      <h2>User List</h2>
      <div style={{ textAlign: "center" }}>
        <Table columns={columns} dataSource={data} rowKey={(record) => Math.random() } />
      </div>
    </div>
  );
};

export default UserView;
