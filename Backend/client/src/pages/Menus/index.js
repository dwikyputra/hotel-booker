import React from "react";
import { Switch } from "react-router-dom";

import RouteWithProps from "../../shared/routes/RouteWithProps";
import routes from "../../routes";

const DashboardMenus = () => {
  return (
    <Switch>
      {
        routes.dashboardmenu.map((menu,i)=>{
          return <RouteWithProps key={i} exact path={menu.path} component={menu.component} extraProps={menu.extraprops} />
        })
      }
      {
        routes.othermenu.map((menu,i)=>{
          return <RouteWithProps key={i} exact path={menu.path} component={menu.component} extraProps={menu.extraprops} />
        })
      }      
    </Switch>
  );
};

export default DashboardMenus;
