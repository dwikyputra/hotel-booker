import React, { useState, useEffect } from "react";
import moment from 'moment';
import { message, Table, Popconfirm, Tag, Button, Modal, Form, Input, DatePicker, Spin   } from "antd";
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { formatCurrency } from '../../shared/utils/Helper'
import { fetchGet, fetchDelete, fetchPut } from '../../shared/utils/API'


const CollectionForm = ({ visible, loading, onUpdate, onCancel, bookingData }) => {
  const [form] = Form.useForm();
  const [dates, setDates] = useState([]);
  const dateFormat = 'YYYY/MM/DD';
  const requiredRange = moment(bookingData.book_checkout_date).diff(moment(bookingData.book_checkin_date), 'days')
  const disabledDate = current => {
    if (!dates || dates.length === 0) {
      return false;
    }
    const tooLate = dates[0] && current.diff(dates[0], 'days') > requiredRange;
    const tooEarly = dates[1] && dates[1].diff(current, 'days') > requiredRange;
    return tooEarly || tooLate;
  };  

  form.setFieldsValue({
    guest_name: bookingData.guest_name,
    booking_date: [moment(bookingData.book_checkin_date, dateFormat), moment(bookingData.book_checkout_date, dateFormat)]
  }) 

  return (
    <Modal
      forceRender
      visible={visible}
      title="Edit Booking Data"
      okText="Submit"
      cancelText="Cancel"
      onCancel={onCancel}
      onOk={() => {
        form
          .validateFields()
          .then(values => {
            form.resetFields();
            onUpdate(bookingData._id, values);
          })
          .catch(info => {
            console.log('Validate Failed:', info);
          });
      }}
    >
      {
        loading 
        ? <Spin/>
        : 
        <Form form={form} layout="vertical" name="form_in_modal" >
          <Form.Item name="guest_name" label="Guest Name" rules={[{ 
            required: true,
            message: 'Please input the guest name!',
          }]}>
            <Input />
          </Form.Item>
          <Form.Item name="booking_date" label="Booking Date" rules={[{ 
            required: true,
            message: 'Please correct booking date!'
          }]}>
            <DatePicker.RangePicker disabledDate={disabledDate} format={dateFormat} onCalendarChange={value=>setDates(value)}/>
          </Form.Item>        
        </Form>
      }
    </Modal>
  );
};


const BookingView = props => {
  const [data, setData] = useState([]);
  const [token, setToken] = useState("");
  const [visible, setVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [bookingData, setBookingData] = useState([]);

  const columns = [
    {
      title: 'Code',
      dataIndex: 'booking_code',
      key: 'booking_code',
      sorter: (a, b) => {
        if (a.booking_code < b.booking_code) return -1
        if (a.booking_code > b.booking_code) return 1;
        return 0;
      },
    },
    {
      title: 'Member',
      dataIndex: 'member',
      key: 'member',
      render: (text, record) => record.member.name,
      defaultSortOrder: 'descend',
      sorter: (a, b) => {
        if (a.member.name < b.member.name) return -1
        if (a.member.name > b.member.name) return 1;
        return 0;
      },
    },
    {
      title: 'Guest',
      dataIndex: 'guest_name',
      key: 'guest_name',
      defaultSortOrder: 'descend',
      sorter: (a, b) => {
        if (a.guest_name < b.guest_name) return -1
        if (a.guest_name > b.guest_name) return 1;
        return 0;
      },
    },  
    {
      title: 'Transaction',
      dataIndex: 'transaction',
      key: 'transaction',
      render: (text, record) => record.transaction.transaction_code,
      defaultSortOrder: 'descend',
      sorter: (a, b) => {
        if (a.transaction.transaction_code < b.transaction.transaction_code) return -1
        if (a.transaction.transaction_code > b.transaction.transaction_code) return 1;
        return 0;
      },
    },    
    {
      title: 'Rooms',
      dataIndex: 'Rooms',
      key: 'rooms',
      sorter: (a, b) => a.rooms.length - b.rooms.length,
      render: (text, record) => (
        <span>
          {record.rooms.map(r => {
            return (
              <Tag color={'geekblue'} key={r.room_number}>
                {r.room_number}
              </Tag>
            );
          })}
        </span>
      ),
    },   
    {
      title: 'Cost',
      dataIndex: 'cost',
      key: 'cost',
      render: (text, record) => '$ '+formatCurrency(record.cost),
      defaultSortOrder: 'descend',
      sorter: (a, b) => a.cost - b.cost,
    },          
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: (text, record) => {
        let color = 'volcano'
        if (record.status === 'checkedin' || record.status === 'checkedout') color = 'green';
        else if (record.status === 'booked' || record.status === 'confirmed') color = 'geekblue'
        return (
          <span>
            <Tag color={color} key={record.status}>
              {record.status.toUpperCase()}
            </Tag>
          </span>
      )},
      sorter: (a, b) => {
        if (a.status < b.status) return -1
        if (a.status > b.status) return 1;
        return 0;
      },      
    }, 
    {
      title: 'Check In Plan',
      dataIndex: 'book_checkin_date',
      key: 'book_checkin_date',
      render: book_checkin_date => moment(book_checkin_date).format('MM/DD/YYYY'),
      defaultSortOrder: 'descend',
      sorter: (a, b) => new Date(a.book_checkin_date).getTime() - new Date(b.book_checkin_date).getTime(),
    },     
    {
      title: 'Check Out Plan',
      dataIndex: 'book_checkout_date',
      key: 'book_checkout_date',
      render: book_checkout_date => moment(book_checkout_date).format('MM/DD/YYYY'),
      defaultSortOrder: 'descend',
      sorter: (a, b) => new Date(a.book_checkout_date).getTime() - new Date(b.book_checkout_date).getTime(),
    },    
    {
      title: 'Action',
      key: 'action',
      render: (text, record) => (
        <span>
            <Popconfirm title="Are you sure want to change this booking data?" onConfirm={() => openFormModal(record)}>
              <Button style={{ marginRight: 16 }} type="primary" shape="circle" icon={<EditOutlined />} />
            </Popconfirm>
            <Popconfirm title="Sure to delete?" onConfirm={() => handleDelete(record._id)}>
              <Button type="primary" shape="circle" danger icon={<DeleteOutlined />} />
            </Popconfirm>
        </span>
      ),
    },
  ];

  useEffect( () => {
    const fetchData = async () => {
      var tempToken = localStorage.getItem("token")
      var res = await fetchGet('bookings', {key:tempToken})
      if(!res.success) message.warning("Get transaction data failed");
      else {
        console.log(res.data)
        setToken(tempToken)
        setData(res.data.data)
      }
    };
    fetchData();
  },[]);

  const handleDelete = async (id) => {
    var res = await fetchDelete('bookings/'+id, {key:token})
    if(!res.success) message.warning("Delete transaction data failed");
    else {
      message.warning("Delete booking data success");
      const dataSource = [...data];
      setData(dataSource.filter(item => item._id !== id))   
    }
  };  

  const openFormModal = async (bookingData) => {
    setBookingData(bookingData)
    setVisible(true);
  }; 

  const onUpdate = async (id, values) => {
    setLoading(true)
    var bookdata = {
      guest_name: values.guest_name,
      book_checkin_date: values.booking_date[0].format('MM/DD/YYYY'),
      book_checkout_date: values.booking_date[1].format('MM/DD/YYYY')
    }
    var res = await fetchPut('bookings/'+id, {key:token}, bookdata)
    if(!res.success || !res.data.data) message.warning("Update booking data failed");
    else {
      setVisible(false);
      message.warning("Update booking data success");
      const dataSource = [...data];
      setData(dataSource.map(item => item._id === id ? {...item, ...bookdata} : item))
      setBookingData(res.data.data)
    }
    setLoading(false)
  };

  return (
    <div style={{ marginTop: 120 }}>
      <h2>Booking List</h2>
      <div style={{ textAlign: "center" }}>
        {
          data.length > 0 
          ? <Table columns={columns} dataSource={data} rowKey={(record) => Math.random() } />
          : <Spin size="large" />
        }
        <CollectionForm
          visible={visible}
          loading={loading}
          onUpdate={onUpdate}
          onCancel={() => {
            setVisible(false);
          }}
          bookingData={bookingData}
        />        
      </div>
    </div>
  );
};

export default BookingView;
  