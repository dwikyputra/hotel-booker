import React from "react";

const NotFoundPage = () => {
  return (
    <>
      <h1>Error 404</h1>
      <span style={{padding:10}}></span>
      <h2> Page Not Found</h2>
    </>
  )
};

export default NotFoundPage;
