import React, { useState, useEffect } from "react";
import { message, Table, Popconfirm, Button, Tag, Row, Spin } from "antd";
import { DeleteOutlined, CloudDownloadOutlined } from '@ant-design/icons';
import { fetchGet, fetchDelete } from '../../shared/utils/API'
import { exportTransactionFileName } from '../../shared/utils/Setting'
import { formatCurrency } from '../../shared/utils/Helper'
import { CSVLink } from 'react-csv'
import moment from "moment";


const TransactionView = props => {
  const [data, setData] = useState([]);
  const [exportData, setExportData] = useState([{bookings:[{id:''}],created_by:'',updated_by:''}]);

  const columns = [
    {
      title: 'Code',
      dataIndex: 'transaction_code',
      key: 'transaction_code',
      sorter: (a, b) => {
        if (a.transaction_code < b.transaction_code) return -1
        if (a.transaction_code > b.transaction_code) return 1;
        return 0;
      },
    },
    {
      title: 'Name',
      dataIndex: 'member',
      key: 'member',
      render: (text, record) => record.member.name,
      defaultSortOrder: 'descend',
      sorter: (a, b) => {
        if (a.member.name < b.member.name) return -1
        if (a.member.name > b.member.name) return 1;
        return 0;
      },
    },
    {
      title: 'Booking',
      dataIndex: 'bookings',
      key: 'bookings',
      sorter: (a, b) => a.bookings.length - b.bookings.length,
      render: bookings => (
        <span>
          {bookings.map(book => {
            return (
              <Tag color={'geekblue'} key={book.booking_code}>
                {book.booking_code.toUpperCase()}
              </Tag>
            );
          })}
        </span>
      ),
    },  
    {
      title: 'Total',
      dataIndex: 'total_cost',
      key: 'total_cost',
      render: (text, record) => '$ '+formatCurrency(record.total_cost),
      defaultSortOrder: 'descend',
      sorter: (a, b) => a.total_cost - b.total_cost,
    },  
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: status => {
        let color = 'volcano'
        if (status === 'paid') color = 'green';
        else if (status === 'pending') color = 'geekblue'
        return (
          <span>
            <Tag color={color} key={status}>
              {status.toUpperCase()}
            </Tag>
          </span>
      )},
    },   
    {
      title: 'Confirmation',
      dataIndex: 'confirmation',
      key: 'confirmation',
      render: (text, record) => {
        return (
          <span style={{color:'green'}}>
            {
              record.is_confirmed? 'Confirmed' : 
              <Popconfirm
                title="Confirm this payment?" 
                onConfirm={() => handleConfirmation(record._id,true)}
                onCancel={() => handleConfirmation(record._id,false)}
                okText="Confirm"
                cancelText="Reject"
              >
                <Button size="small" danger> Proceed Confirmation </Button>
              </Popconfirm>
            }
          </span>
      )},
    },     
    {
      title: 'Action',
      key: 'action',
      render: (text, record) => (
        <span>
          {/* <Button style={{ marginRight: 16 }} type="primary" shape="circle" icon={<EditOutlined />} /> */}
          <Popconfirm title="Sure to delete?" onConfirm={() => handleDelete(record._id)}>
            <Button type="primary" shape="circle" danger icon={<DeleteOutlined />} />
          </Popconfirm>
        </span>
      ),
    },
  ];  

  useEffect( () => {
    const fetchData = async () => {
      var token = localStorage.getItem("token")
      var res = await fetchGet('transactions', {key:token})
      if(!res.success) message.warning("Get transaction data failed");
      else {
        setData(res.data.data)
        const dataSource = [...res.data.data];
        setExportData(dataSource.map(item => { return {
          ...item,
          member:item.member.name,
          bookings:item.bookings.map(b=>b.booking_code),
          created_by:item.created_by.name,
          create_date:moment(item.create_date).format('MM/DD/YYYY HH:mm:ss'),
          updated_by:item.updated_by.name,
          updated_date:moment(item.updated_date).format('MM/DD/YYYY HH:mm:ss')
        }}))   
      }
    };
    fetchData();
  },[]);

  const handleDelete = async (id) => {
    var token = localStorage.getItem("token")
    var res = await fetchDelete('transactions/'+id, {key:token})
    if(!res.success) message.warning("Delete transaction data failed");
    else {
      message.warning("Delete transaction data success");
      const dataSource = [...data];
      setData(dataSource.filter(item => item._id !== id))   
    }
  };  

  const handleConfirmation = async (id, val) => {
    var token = localStorage.getItem("token")
    var res = await fetchGet('confirmation/'+id+'/'+(val?1:0), {key:token})
    if(!res.success) message.warning("Transaction confirmation data failed");
    else {
      message.success("Transaction confirmation data success");
      const dataSource = [...data];
      setData(dataSource.map(item => item._id === id ? {...item, is_confirmed:val, status:val?'paid':'rejected'} : item))
    }
  };   

  const ExportButton = () => {
    return (
      <Button type="primary" shape="round" icon={<CloudDownloadOutlined />} style={{backgroundColor:'orange', borderColor:'orange'}}>
        <CSVLink data={exportData} separator={";"} filename={exportTransactionFileName} className="export-btn">Export</CSVLink>
      </Button>      
    )
  }

  return (
    <div style={{ marginTop: 120 }}>
      <Row justify="space-between" style={{marginBottom:10}}>
        <h2>Transaction List</h2>
        <ExportButton/>
      </Row>
      <div style={{ textAlign: "center" }}>
        {
          data.length > 0 
          ? <Table columns={columns} dataSource={data} rowKey={(record) => Math.random() } />
          : <Spin size="large" />
        }
      </div>
    </div>
  );
};

export default TransactionView;
