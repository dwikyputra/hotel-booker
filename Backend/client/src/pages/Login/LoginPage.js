import React, {useState} from "react";
import { message, Card, Form, Input, Button } from "antd";

import {fetchPost} from '../../shared/utils/API'

const LoginPage = props => {
  const [email, setMail] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmit = async () => {
    var res = await fetchPost('signin', {}, {email:email, password:password})
    if(!res.success || !res.data.data) message.warning("Login failed");
    else {
      message.warning("Login success");
      localStorage.setItem("token", res.data.data.token); 
      localStorage.setItem("userId", res.data.data.user._id); 
      console.log('login',res.data.data.user._id)
      props.history.push('/');
    }
  };
  
  return (
    <>
      <Card
        bordered={false}
        style={{
          border: "1px solid #dcdcdc",
          boxShadow: "0px 15px 20px 5px #0000001a",
          width: 400
        }}
      >
        <h1 style={{ textAlign: "center" }}>HotelBooker Login</h1>
        <Form
          hideRequiredMark
          colon={false}
          onSubmitCapture={()=>handleSubmit()}
          layout="vertical"
        >
          <Form.Item label="Email" name="email" rules={[{ required: true, message: "Email required" }]}>
            <Input size="large" placeholder="John@example.com" onChange={e => setMail(e.target.value)}/>
          </Form.Item>
          <Form.Item label="Password" name="password" rules={[{ required: true, message: "Password required" }]}>
            <Input.Password
                size="large"
                type="password"
                placeholder="Password"
                onChange={e => setPassword(e.target.value)}
              />
          </Form.Item>
          <Button block size="large" htmlType="submit" >
            Login
          </Button>
        </Form>
      </Card>
    </>
  );
};

export default LoginPage;
