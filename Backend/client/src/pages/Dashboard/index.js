import React from "react";
import { Alert, Row } from "antd";

const DashboardView = props => {
  return (
    <div>
      {/* Title of page */}
      <Row
        type="flex"
        justify="center"
        style={{ margin: "120px 0px 32px 0px" }}
      >
        <Alert
          message="Dashboard Page"
          description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem nisi, finibus id enim a, auctor vulputate lacus"
          type="info"
        />
      </Row>
    </div>
  );
};

export default DashboardView;
