import React, { useState, useEffect } from 'react';
import { message, Descriptions, Comment, Form, Input, List, Button, Avatar, Spin } from 'antd';
import moment from 'moment';
import { useParams } from "react-router-dom";

import { fetchGet, fetchPost } from '../../shared/utils/API'


const SingleMailBox = props => {
  let { id } = useParams();

  const [mail, setMailData] = useState({sender:{},recipient:{},type:'-'});
  const [replies, setReplyData] = useState([]);
  const [messageVal, setMessageVal] = useState("");
  const [loading, setLoading] = useState(false);

  useEffect( () => {
    const fetchData = async () => {
      setLoading(true)
      var token = localStorage.getItem("token")
      var userId = localStorage.getItem("userId")
      var res = await fetchGet('mails/'+id, {key:token})
      if(!res.success) message.warning("Get email data failed");
      else {
        var dataSource = {...res.data.data.mail}
        var type = dataSource.recipient._id === userId ? 'Inbox' : 'Outbox'
        setMailData({...dataSource, type})
        setReplyData(res.data.data.reply.map(m=>{
          return {
            author: m.title,
            avatar: <Avatar size="large">{m.index}</Avatar>,
            content: m.content,
            datetime: moment(m.create_date).fromNow(),
            actions: [
              <span >Sent By: </span>,
              <span><span style={{textTransform:"capitalize"}}>{m.sender.name}</span> - ({m.sender.email})</span>
            ]
          }
        }))
      }
      setLoading(false)
    }; 
    fetchData();
  },[id]);

  const handleMessageChange = e => setMessageVal(e.target.value);

  const handleSubmit = async () => {
    if (!messageVal) return;
    setLoading(true)
    var token = localStorage.getItem("token")
    var res = await fetchPost('mails', {key:token}, {
      sender: mail.recipient._id,
      recipient: mail.sender._id,
      parent_mail: mail._id,
      index: replies.length+1,
      title: "Re - "+mail.title,
      content: messageVal
    })
    if(!res.success || !res.data.data) message.warning("Send reply failed");
    else {
      message.warning("Send reply success");
      setLoading(false)
      setMessageVal("")
      setReplyData([
        ...replies,
        {
          avatar: <Avatar size="large">{replies.length+1}</Avatar>,
          author: "Re - "+mail.title,
          content: <p>{messageVal}</p>,
          datetime: moment().fromNow(),
          actions: [
            <span >Sent By: </span>,
            <span><span style={{textTransform:"capitalize"}}>{mail.recipient.name}</span> - ({mail.sender.email})</span>
          ]
        },
      ])
    }
  };

  const ReplyList = ({ comments }) => (
    <List
      dataSource={comments}
      header={`${comments.length} ${comments.length > 1 ? 'replies' : 'reply'}`}
      itemLayout="horizontal"
      renderItem={props => <Comment {...props} />}
    />
  );

  return (
    <>
      {
        loading
        ? <Spin size="large" />
        : <>
            <div className="site-layout-background">
              <Descriptions title={`${mail.type.replace(/^./, mail.type[0].toUpperCase())} - ${mail.title}`} layout="vertical" style={{marginBottom:30}}>
                <Descriptions.Item label="Name">{mail.type==='Inbox'?mail.sender.name:mail.recipient.name}</Descriptions.Item>
                <Descriptions.Item label="Email">{mail.type==='Inbox'?mail.sender.email:mail.recipient.email}</Descriptions.Item>
                <Descriptions.Item label="Timestamp">{moment(mail.create_date).format('YYYY-MM-DD HH:mm:ss')}</Descriptions.Item>
              </Descriptions>

              <Descriptions layout="vertical">
                <Descriptions.Item label="Content" span={4}>
                {mail.content}
                </Descriptions.Item>
              </Descriptions>          
            </div>    
            <div className="site-layout-background">
        {replies.length > 0 && <ReplyList comments={replies} />}
        <Comment
          content={
            <div>
              <Form.Item>
                <Input.TextArea rows={4} onChange={handleMessageChange} value={messageVal} />
              </Form.Item>
              <Form.Item>
                <Button htmlType="submit" loading={loading} onClick={handleSubmit} type="primary">
                  Send Reply
                </Button>
              </Form.Item>           
            </div>
          }
        />
      </div>         
          </>
      }
    </>
  );
}

export default SingleMailBox;
