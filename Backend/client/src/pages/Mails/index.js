import React, { useState, useEffect } from "react";
import moment from 'moment'
import { message, Table, Popconfirm, Button, Row, Form, Input, Modal, Select, Spin  } from "antd";
import { DeleteOutlined, EyeOutlined, PlusCircleOutlined } from '@ant-design/icons';
import { fetchGet, fetchDelete, fetchPost } from '../../shared/utils/API'


const ModalForm = ({ visible, onCreate, onCancel, data }) => {
  const [form] = Form.useForm();
  const options = data.map(d => <Select.Option key={d._id}>{d.name}</Select.Option>);
  return (
    <Modal
      forceRender
      visible={visible}
      title="Compose Mail"
      okText="Submit"
      cancelText="Cancel"
      onCancel={onCancel}
      onOk={() => {
        form
          .validateFields()
          .then( async values => {
            var res = await onCreate(values);
            if(res) form.resetFields();
          })
          .catch(info => {
            console.log('Validate Failed:', info);
          });
      }}
    >
      <Form form={form} layout="vertical" name="form_in_modal" >
        <Form.Item name="recipient" label="To" required>
          <Select
            showSearch
            filterOption={(input, option) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
          >
            {options}
          </Select>
        </Form.Item> 
        <Form.Item name="title" label="Title" rules={[{ 
          required: true,
          message: 'Please input the title!',
        }]}>
          <Input />
        </Form.Item>     
        <Form.Item name="content" label="Content">
          <Input.TextArea />
        </Form.Item>           
      </Form>
    </Modal>
  );
};


const MailView = props => {
  const [data, setData] = useState([]);
  const [users, setUsers] = useState([]);
  const [token, setToken] = useState("");
  const [userId, setUserId] = useState("");
  const [visible, setVisible] = useState(false);

  const columns = [
    {
      title: 'Type',
      dataIndex: 'type',
      key: 'type',
      render: (text, record) => record.type.replace(/^./, record.type[0].toUpperCase()),
      sorter: (a, b) => {
        if (a.type < b.type) return -1
        if (a.type > b.type) return 1;
        return 0;
      },
    },    
    {
      title: 'Sender',
      dataIndex: 'sender',
      key: 'sender',
      render: (text, record) => record.sender.name,
      sorter: (a, b) => {
        if (a.sender.name < b.sender.name) return -1
        if (a.sender.name > b.sender.name) return 1;
        return 0;
      },
    },
    {
      title: 'Recipient',
      dataIndex: 'recipient',
      key: 'recipient',
      render: (text, record) => record.recipient.name,
      sorter: (a, b) => {
        if (a.recipient.name < b.recipient.name) return -1
        if (a.recipient.name > b.recipient.name) return 1;
        return 0;
      },
    },    
    {
      title: 'Email',
      dataIndex: 'sender',
      key: 'sender',
      render: (text, record) => record.sender.email,
      defaultSortOrder: 'descend',
      sorter: (a, b) => {
        if (a.sender.email < b.sender.email) return -1
        if (a.sender.email > b.sender.email) return 1;
        return 0;
      },
    },
    {
      title: 'Title',
      dataIndex: 'title',
      key: 'title',
      sorter: (a, b) => {
        if (a.title < b.title) return -1
        if (a.title > b.title) return 1;
        return 0;
      },
    },  
    {
      title: 'Sent Date',
      dataIndex: 'create_date',
      key: 'create_date',
      render: create_date => moment(create_date).format('MM/DD/YYYY'),
      defaultSortOrder: 'descend',
      sorter: (a, b) => new Date(a.create_date).getTime() - new Date(b.create_date).getTime(),
    },      
    {
      title: 'Action',
      key: 'action',
      render: (text, record) => (
        <span>
          <Button style={{ marginRight: 16 }} type="primary" shape="circle"
            onClick={()=>props.history.push('mail/'+record._id)} icon={<EyeOutlined />} />
          <Popconfirm title="Sure to delete?" onConfirm={() => handleDelete(record._id)}>
            <Button type="primary" shape="circle" danger icon={<DeleteOutlined />} />
          </Popconfirm>
        </span>
      ),
    },
  ];

  useEffect( () => {
    const fetchData = async () => {
      var tempToken = localStorage.getItem("token")
      var tempUserId = localStorage.getItem("userId")
      var res = await fetchGet('mails', {key:tempToken})
      if(!res.success) message.warning(`Get mail data failed`);
      else {
        const dataSource = [...res.data.data];
        setData(dataSource.map(item => item.recipient._id === tempUserId ? {...item, type:'inbox'} : {...item, type:'outbox'}))        
        setToken(tempToken)
        setUserId(tempUserId)
      }
    };
    fetchData();
  },[]);

  const handleDelete = async (id) => {
    var res = await fetchDelete('mails/'+id, {key:token})
    if(!res.success) message.warning("Delete mail data failed");
    else {
      message.warning("Delete mail data success");
      const dataSource = [...data];
      setData(dataSource.filter(item => item._id !== id),)   
    }
  };  

  const openFormModal = async () => {
    var res = await fetchGet('users', {key:token})
    if(!res.success) message.warning(`Get users data failed`);
    else {
      setUsers(res.data.data)
      setVisible(true);
    }
  }; 

  const onCreate = async (values) => {
    var createdata = {
      sender: userId,
      recipient: values.recipient,
      title: values.title,
      content: values.content
    }
    var res = await fetchPost('mails', {key:token}, createdata)
    if(!res.success || !res.data.data) {
      message.warning("Compose mail data failed");
      return false
    } else {
      setVisible(false);
      message.warning("Compose mail data success");
      var mailData = {...res.data.data}
      var type = mailData.recipient._id === userId ? 'Inbox' : 'Outbox'
      setData([...data, {...mailData, type}])
      return true
    }
  };

  return (
    <div style={{ marginTop: 120 }}>
      <Row justify="space-between" style={{marginBottom:10}}>
        <h2>Mailbox List</h2>
        <Button type="primary" shape="round" icon={<PlusCircleOutlined />} size={'large'} onClick={openFormModal}>
          Compose Mail
        </Button>
      </Row>
      <div style={{ textAlign: "center" }}>
        {
          data.length > 0 
          ? <Table columns={columns} dataSource={data} rowKey={(record) => Math.random() } />
          : <Spin size="large" />
        }
      </div>
      <ModalForm
        visible={visible}
        onCreate={onCreate}
        onCancel={() => {
          setVisible(false);
        }}
        data={users}
      />        
    </div>
  );
};

export default MailView;
