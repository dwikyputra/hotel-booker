module.exports = {
  apps : [{
    name: 'hotel-booker',
    script: 'npx',

    // Options reference: https://pm2.keymetrics.io/docs/usage/application-declaration/
    args: 'serve -s -l 8448 build',
   // args: 'npm start',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }],

  deploy : {
    production : {
      user : 'node',
      host : '206.189.189.243',
      ref  : 'origin/master',
      repo : 'git@github.com:repo.git',
      path : '/var/www/production',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
    }
  }
};
