Mail = require('../models/mail.model');
crypto = require('crypto')

// Handle all actions
exports.all = async function (req, res) {
  var user = res.user
  try {
    const mails = await Mail.find({$or:[
      { recipient:user._id },
      { sender:user._id }
    ], parent_mail:null}).populate('sender recipient')
    res.json({ status: 200, message: "Mails retrieved successfully", data: mails })
  } catch (err) {
    res.json({ status: 400, message: err.message })
  }
};

// Handle create mail actions
exports.create = async (req, res) => {
  const mail = new Mail()
  var fields = Object.keys(req.body)
  fields.map(field=>{
    if (req.body[field] != null) {
      mail[field] = req.body[field]
    }
  })
  mail.created_by = res.user._id
  try {
    var newMail = await mail.save()
    newMail = await newMail.populate('sender recipient').execPopulate()
    res.json({ status: 200, message: 'New mail created' , data: newMail })
  } catch (err) {
    res.json({ status: 400, message: err.message })
  }
};

// Handle view mail info
exports.read = async (req, res) => {
  try {
    const re_mails = await Mail.find({parent_mail:res.mail._id}).sort('index').populate('sender recipient')
    res.json({ status: 200, message: "Mail details", data: {
      mail: res.mail,
      reply: re_mails
    }})
  } catch (err) {
    res.json({ status: 400, message: err.message })
  }
};

// Handle update mail info
exports.update = async (req, res) => {
  var fields = Object.keys(req.body)
  fields.map(field=>{
    if (req.body[field] != null) {
      res.mail[field] = req.body[field]
    }
  })
  res.mail.updated_by = res.user._id
  res.mail.updated_date = Date.now()
  try {
    const updatedData = await res.mail.save()
    res.json({ status: 200, message: 'Mail data updated', data: updatedData })
  } catch {
    res.json({ status: 400, message: err.message })
  }
};

// Handle delete mail
exports.delete = async (req, res) => {
  try {
    await res.mail.remove()
    await Mail.deleteMany({ parent_mail:res.mail._id })
    res.json({ status: 200, message: 'Mail deleted', data: res.mail })
  } catch(err) {
    res.json({ status: 400, message: err.message })
  }  
};