RoomSchedule = require('../models/room_schedule.model');
Room = require('../models/room.model');
crypto = require('crypto')

// Handle all actions
exports.all = async function (req, res) {
  try {
    const room_schedules = await RoomSchedule.find()
    res.json({ status: 200, message: "RoomSchedules retrieved successfully", data: room_schedules })
  } catch (err) {
    res.json({ status: 400, message: err.message })
  }
};

// Handle get schedule by fields
exports.scheduleByDateAndType = async function (req, res) {
  try {
    const {startdate, enddate, roomtype} = req.body
    const room_schedules = await RoomSchedule.find({ 
      room_type: roomtype,
      book_date: {
        $gte: moment(startdate).startOf('day'),
        $lt: moment(enddate).endOf('day')
      }
    }).sort({ book_date: 'asc'})  
    var rooms_taken = room_schedules.map(rs=>rs.room.toString())
    var rooms = await Room.find({room_type:roomtype})
    var rooms_available = rooms.map(r => {
      return {
        _id:r._id,
        room_number:r.room_number,
        status:rooms_taken.includes(r._id.toString())?'taken':'available'
      }
    })
    res.json({ status: 200, message: "Room schedules retrieved successfully", data: rooms_available })
  } catch (err) {
    res.json({ status: 400, message: err.message })
  }
};

// Handle delete room
exports.delete = async (req, res) => {
  try {
    var schedule = await RoomSchedule.findById(req.params.id)
    await schedule.remove()
    res.json({ message: 'Room schedule deleted', data: schedule })
  } catch(err) {
    res.json({ status: 400, message: err.message })
  }  
};