User = require('../models/user.model');
crypto = require('crypto')

// Handle all actions
exports.all = async function (req, res) {
  try {
    const users = await User.find()
    res.json({ status: 200, message: "Users retrieved successfully", data: users })
  } catch (err) {
    res.json({ status: 400, message: err.message })
  }
};

// Handle create user actions
exports.create = async (req, res) => {
  const user = new User({
    name: req.body.name,
    password: crypto.createHash('md5').update(req.body.password).digest('hex'),
    gender: req.body.gender,
    email: req.body.email,
    phone: req.body.phone,
    role: req.body.role
  })

  try {
    const newUser = await user.save()
    res.json({ status: 200, message: 'New user created' , data: newUser })
  } catch (err) {
    res.json({ status: 400, message: err.message })
  }
};

// Handle view user info
exports.read = async (req, res) => {
  res.json({ status: 200, message: "User details", data: res.user })
};

// Handle update user info
exports.update = async (req, res) => {
  var fields = Object.keys(req.body)
  fields.map(field=>{
    if (req.body[field] != null) {
      if (field == "password") res.user[field] = crypto.createHash('md5').update(req.body[field]).digest('hex')
      else res.user[field] = req.body[field]
    }
  })
  res.user.updated_by = res.user._id
  res.user.updated_date = Date.now()  
  try {
    const updatedData = await res.user.save()
    res.json({ status: 200, message: 'User data updated', data: updatedData })
  } catch {
    res.json({ status: 400, message: err.message })
  }
};

// Handle delete user
exports.delete = async (req, res) => {
  try {
    await res.user.remove()
    res.json({ status: 200, message: 'User deleted', data: res.user })
  } catch(err) {
    res.json({ status: 400, message: err.message })
  }  
};