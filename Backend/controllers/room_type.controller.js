RoomTypeType = require('../models/room_type.model');
crypto = require('crypto')

// Handle all actions
exports.all = async function (req, res) {
  try {
    const room_types = await RoomType.find()
    res.json({ status: 200, message: "Room type retrieved successfully", data: room_types })
  } catch (err) {
    res.json({ status: 400, message: err.message })
  }
};

// Handle create room_type actions
exports.create = async (req, res) => {
  const room_type = new RoomType()
  var fields = Object.keys(req.body)
  fields.map(field=>{
    if (req.body[field] != null) {
      room_type[field] = req.body[field]
    }
  })
  room_type.created_by = res.user._id
  try {
    const newRoomType = await room_type.save()
    res.json({ status: 200, message: 'New room type created' , data: newRoomType })
  } catch (err) {
    res.json({ status: 400, message: err.message })
  }
};

// Handle view room_type info
exports.read = async (req, res) => {
  res.json({ status: 200, message: "Room type details", data: res.room_type })
};

// Handle update room_type info
exports.update = async (req, res) => {
  var fields = Object.keys(req.body)
  fields.map(field=>{
    if (req.body[field] != null) {
      res.room_type[field] = req.body[field]
    }
  })
  res.room_type.updated_by = res.user._id
  res.room_type.updated_date = Date.now()
  try {
    const updatedData = await res.room_type.save()
    res.json({ status: 200, message: 'Room type data updated', data: updatedData })
  } catch {
    res.json({ status: 400, message: err.message })
  }
};

// Handle delete room_type
exports.delete = async (req, res) => {
  try {
    await res.room_type.remove()
    res.json({ status: 200, message: 'Room type deleted', data: res.room_type })
  } catch(err) {
    res.json({ status: 400, message: err.message })
  }  
};