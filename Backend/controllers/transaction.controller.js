Transaction = require('../models/transaction.model');
Booking = require('../models/booking.model');
RoomSchedule = require('../models/room_schedule.model');
Room = require('../models/room.model');
crypto = require('crypto')
moment = require('moment')


function ID() {
  return Math.random().toString(36).substr(2, 6).toUpperCase()
}

// Handle all actions
exports.all = async function (req, res) {
  try {
    const transactions = await Transaction.find().populate('member bookings created_by updated_by')
    res.json({ status: 200, message: "Transaction retrieved successfully", data: transactions })
  } catch (err) {
    res.json({ status: 400, message: err.message })
  }
};

// Handle create transaction actions
exports.create = async (req, res) => {
  const transaction = new Transaction()
  var fields = Object.keys(req.body)
  fields.map(field=>{
    if (req.body[field] != null) {
      transaction[field] = req.body[field]
    }
  })
  transaction.created_by = res.user._id
  try {
    const newTransaction = await transaction.save()
    res.json({ status: 200, message: 'New transaction created' , data: newTransaction })
  } catch (err) {
    res.json({ status: 400, message: err.message })
  }
};

// Handle view transaction info
exports.read = async (req, res) => {
  res.json({ status: 200, message: "Transaction details", data: res.transaction })
};

// Handle update transaction info
exports.update = async (req, res) => {
  var fields = Object.keys(req.body)
  fields.map(field=>{
    if (req.body[field] != null) {
      res.transaction[field] = req.body[field]
    }
  })
  res.transaction.updated_by = res.user._id
  res.transaction.updated_date = Date.now()
  try {
    const updatedData = await res.transaction.save()
    res.json({ status: 200, message: 'Transaction data updated', data: updatedData })
  } catch {
    res.json({ status: 400, message: err.message })
  }
};

// Handle delete transaction
exports.delete = async (req, res) => {
  try {
    await res.transaction.remove()
    await Booking.deleteMany({ _id:{$in:res.transaction.bookings} })
    await RoomSchedule.deleteMany({ transaction:res.transaction._id })
    res.json({ status: 200, message: 'Transaction deleted', data: res.transaction })
  } catch(err) {
    res.json({ status: 400, message: err.message })
  }  
};

// Handle checkout cart actions
exports.checkout = async (req, res) => {
  try {
    const transaction = new Transaction({
      transaction_code: "TR-"+ID(),
      member: res.user._id,
      created_by: res.user._id
    })
    const newTransaction = await transaction.save()
    let bookings = await Promise.all(
      req.body.cartdata.map(async cd => {
        var room_type = await RoomType.findById(cd.roomtype)
        let rooms = await Room.find({room_type:cd.roomtype})
        var room_ids_avail = await getRoomScheduleByType({...cd},rooms)
        var booking = new Booking({
          rooms: room_ids_avail,
          room_type: room_type._id,
          member: res.user._id,
          transaction: newTransaction._id,
          booking_code: "BO-"+ID(),
          cost: calculateTotalPrice(cd,room_type.price),
          guest_name: cd.guestName!="" ? cd.guestName : res.user.name,
          book_checkin_date: cd.startdate,
          book_checkout_date: cd.enddate,
          created_by: res.user._id          
        })
        let newBooking = await booking.save() 
        let scheduleData = {
          diffDate: moment(cd.enddate).diff(moment(cd.startdate), 'days')+1,
          checkinDate: cd.book_checkin_date,
          oData: {
            transaction: newTransaction._id,
            booking: newBooking._id,
            room_type: room_type._id,
            member: res.user._id
          }
        }
        await createRoomSchedule(scheduleData, room_ids_avail)
        return newBooking
      })
    )
    var transactionUpd = await Transaction.findByIdAndUpdate(newTransaction._id,{
      total_cost : bookings.reduce((acc, cur) => acc + cur.cost, 0),
      bookings : bookings.map(b=>b._id),
      updated_by : res.user._id,
      updated_date : Date.now()
    })
    res.json({ status: 200, message: 'Checkout process success' , data: {transaction_code : transactionUpd.transaction_code} })
  } catch (err) {
    res.json({ status: 400, message: err.message })
  }
};

const calculateTotalPrice = (item, roomtypeprice) => {
  var diff = moment(item.enddate).diff(moment(item.startdate),'days')
  return roomtypeprice*item.numRoom*diff
}

async function getRoomScheduleByType(data, rooms) {
  try {
    const {startdate, enddate, roomtype, numRoom} = data
    const room_schedules = await RoomSchedule.find({ 
      room_type: roomtype,
      book_date: {
        $gte: moment(startdate).startOf('day'),
        $lt: moment(enddate).endOf('day')
      }
    }).sort({ book_date: 'asc'})  
    var rooms_taken = room_schedules.map(rs=>rs.room.toString())
    var rooms_avail = rooms.filter(r=>!rooms_taken.includes(r._id.toString())).slice(0,numRoom)
    return rooms_avail.map(ra=>ra._id)
  } catch(err) {
    res.json({ status: 400, message: err.message })
  }
  return true
}

async function createRoomSchedule(data, rooms_avail) {
  try {
    await Promise.all(
      rooms_avail.map(async id => {
        let schedules = await Promise.all(
          [...Array(data.diffDate).keys()].map((i) => { 
            return { ...data.oData, room:id, book_date: moment(data.checkinDate).add(i,'days').format('MM/DD/YYYY')}
          })
        )
        await RoomSchedule.create(schedules)
      })
    )
  } catch(err) {
    res.json({ status: 400, message: err.message })
  }
  return true
}

// Handle confirmation transaction
exports.confirmation = async (req, res) => {
  var is_confirmed = req.params.val == 1 ? true : false
  res.transaction.is_confirmed = true
  res.transaction.status = is_confirmed ? 'paid' : 'rejected'
  res.transaction.updated_by = res.user._id
  res.transaction.updated_date = Date.now()
  try {
    const updatedData = await res.transaction.save()
    await Booking.updateMany({_id: {$in: res.transaction.bookings}}, {
      $set: { status: is_confirmed ? 'confirmed' : 'rejected'}
    })
    res.json({ status: 200, message: 'Transaction confirmation success', data: updatedData })
  } catch {
    res.json({ status: 400, message: err.message })
  }
};

const stripe = require('stripe')('sk_test_IDqAsGJjQ4njzaCjjdQo2OaL000DLOJjAs')
// Handle complete payment with stripe
exports.completePaymentWithStripe = (req, res) => {
  console.log(req.body)
  stripe.charges
    .create({
      amount: req.body.amount,
      currency: req.body.currency,
      source: 'tok_mastercard',
    })
    .then(charge => {
      console.log('payment success')
      res.json({ status: 200, message: 'payment success', data:charge })
    })
    .catch(error => {
      console.log(error)
      res.json({ status: 400, message: error })
    })
};