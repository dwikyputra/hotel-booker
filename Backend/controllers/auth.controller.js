const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId

Session = require('../models/session.model');
crypto = require('crypto')

exports.signin = async (req, res) => {
  var user = res.user
  var sessionData = {
    token: crypto.createHash('md5').update(req.sessionID + (new Date())).digest('hex'),
    user: user._id,
  }
  var options = {
    upsert: true,
    new: true,
    setDefaultsOnInsert: true
  }
  try {
    const newSession = await Session.findOneAndUpdate({user: ObjectId(user._id)}, sessionData, options)
    res.json({ status: 200, message: 'Sign in success' , data: {
      token: newSession.token,
      user: user
    }})
  } catch (err) {
    res.json({ status: 400, message: err.message })
  }
}



exports.testFCM = async (req, res) => {
  var ret = {body: req.body, header:req.headers }
  console.log(ret)
  res.json({ status: 200, data: ret})
}