const { check, validationResult } = require('express-validator');

Booking = require('../models/booking.model');
Room = require('../models/room.model');
crypto = require('crypto')


function ID() {
  return Math.random().toString(36).substr(2, 6).toUpperCase()
}

// Handle form validation
exports.validate = (method) => {
  switch (method) {
    case 'create': {
     return [ 
        check('member').not().isEmpty(),
        check('room').not().isEmpty(),
        check('guest_name').not().isEmpty(),
        check('book_checkin_date').not().isEmpty(),
        check('book_checkout_date').not().isEmpty()
       ]   
    }
  }
}

// Handle all actions
exports.all = async function (req, res) {
  try {
    const bookings = await Booking.find().populate('member transaction rooms')
    res.json({ status: 200, message: "Booking retrieved successfully", data: bookings })
  } catch (err) {
    res.json({ status: 400, message: err.message })
  }
};

// Handle all actions
exports.allByUser = async function (req, res) {
  var user = res.user
  try {
    const bookings = await Booking.find({member:user._id}).populate('transaction rooms room_type')
    res.json({ status: 200, message: "Booking retrieved successfully", data: bookings })
  } catch (err) {
    res.json({ status: 400, message: err.message })
  }
};

// Handle create booking actions
exports.create = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.json({ status: 400, errors: errors.array() });
    return;
  }
  try {
    const booking = new Booking()
    var room = room = await Room.findById(req.body.room).populate('room_type')
    var fields = Object.keys(req.body)
    fields.map(field=>{
      if (req.body[field] != null) {
        booking[field] = req.body[field]
      }
    })
    booking.booking_code = ID();
    booking.cost = room.room_type.price;
    booking.created_by = res.user._id
  
    const newBooking = await booking.save()
    res.json({ status: 200, message: 'New booking created' , data: newBooking })
  } catch (err) {
    res.json({ status: 400, message: err.message })
  }
};

// Handle view booking info
exports.read = async (req, res) => {
  res.json({ status: 200, message: "Booking details", data: res.booking })
};

// Handle update booking info
exports.update = async (req, res) => {
  var fields = Object.keys(req.body)
  fields.map(field=>{
    if (req.body[field] != null) {
      res.booking[field] = req.body[field]
    }
  })
  res.booking.updated_by = res.user._id
  res.booking.updated_date = Date.now()
  try {
    const updatedData = await res.booking.save()
    res.json({ status: 200, message: 'Booking data updated', data: updatedData })
  } catch {
    res.json({ status: 400, message: err.message })
  }
};

// Handle delete booking
exports.delete = async (req, res) => {
  try {
    await res.booking.remove()
    await RoomSchedule.deleteMany({ booking:res.booking._id })
    res.json({ status: 200, message: 'Booking deleted', data: res.booking })
  } catch(err) {
    res.json({ status: 400, message: err.message })
  }  
};