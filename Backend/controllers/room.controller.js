Room = require('../models/room.model');
crypto = require('crypto')

// Handle all actions
exports.all = async function (req, res) {
  try {
    const rooms = await Room.find().populate('room_type')
    res.json({ status: 200, message: "Rooms retrieved successfully", data: rooms })
  } catch (err) {
    res.json({ status: 400, message: err.message })
  }
};

// Handle create room actions
exports.create = async (req, res) => {
  const room = new Room()
  var fields = Object.keys(req.body)
  fields.map(field=>{
    if (req.body[field] != null) {
      room[field] = req.body[field]
    }
  })
  room.created_by = res.user._id
  try {
    const newRoom = await room.save()
    res.json({ status: 200, message: 'New room created' , data: newRoom })
  } catch (err) {
    res.json({ status: 400, message: err.message })
  }
};

// Handle view room info
exports.read = async (req, res) => {
  res.json({ status: 200, message: "Room details", data: res.room })
};

// Handle update room info
exports.update = async (req, res) => {
  var fields = Object.keys(req.body)
  fields.map(field=>{
    if (req.body[field] != null) {
      res.room[field] = req.body[field]
    }
  })
  res.room.updated_by = res.user._id
  res.room.updated_date = Date.now()
  try {
    const updatedData = await res.room.save()
    res.json({ status: 200, message: 'Room data updated', data: updatedData })
  } catch {
    res.json({ status: 400, message: err.message })
  }
};

// Handle delete room
exports.delete = async (req, res) => {
  try {
    await res.room.remove()
    res.json({ status: 200, message: 'Room deleted', data: res.room })
  } catch(err) {
    res.json({ status: 400, message: err.message })
  }  
};