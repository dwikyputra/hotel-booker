const mongoose = require('mongoose');

let RoomScheduleSchema = mongoose.Schema({
  transaction: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'transaction',
  },  
  booking: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'booking',
  },    
  room_type: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'roomtype'
  },
  room: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'room'
  },
  member: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'member',
  },     
  status:{
    type: String,
    enum: ['available', 'taken'],
    default: 'taken'
  },
  book_date: Date,
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  create_date: {
    type: Date,
    default: Date.now
  },
  updated_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  updated_date: Date, 
})

module.exports = mongoose.model('roomschedule', RoomScheduleSchema )
