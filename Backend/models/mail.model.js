const mongoose = require('mongoose');

let MailSchema = mongoose.Schema({
  sender: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  recipient: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  parent_mail: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'mail',
    default: null
  },
  index: {
    type: Number,
    default: 0,
  },
  title: String,
  content: String,
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  create_date: {
    type: Date,
    default: Date.now
  },
  updated_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  updated_date: Date, 
})

module.exports = mongoose.model('mail', MailSchema )
