const mongoose = require('mongoose');

let TransactionSchema = mongoose.Schema({
  transaction_code: String,
  member: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  bookings: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'booking',
  }],  
  total_cost: {
    type: Number,
    default: 0
  },
  status:{
    type: String,
    enum: ['unpaid', 'pending', 'paid', 'rejected', 'canceled'],
    default: 'pending'
  },
  is_confirmed: {
    type: Boolean,
    default: false
  },
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  create_date: {
    type: Date,
    default: Date.now
  },
  updated_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  updated_date: Date, 
})

module.exports = mongoose.model('transaction', TransactionSchema )
