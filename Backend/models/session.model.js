const mongoose = require('mongoose');

let SessionSchema = mongoose.Schema({
  token: String,
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  create_date: {
    type: Date,
    default: Date.now
  }
})

module.exports = mongoose.model('session', SessionSchema )
