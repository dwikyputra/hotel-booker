const mongoose = require('mongoose');

let BookingSchema = mongoose.Schema({
  booking_code: String,
  member: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  transaction: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'transaction',
  },  
  room_type: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'roomtype',
  },   
  rooms: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'room',
  }],
  cost: Number,
  status:{
    type: String,
    enum: ['booked', 'confirmed', 'checkedin', 'checkedout', 'rejected', 'canceled'],
    default: 'booked'
  },
  guest_name: String,
  book_checkin_date: Date,
  book_checkout_date: Date,
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  create_date: {
    type: Date,
    default: Date.now
  },
  updated_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  updated_date: Date, 
})

module.exports = mongoose.model('booking', BookingSchema )
