const mongoose = require('mongoose');

let RoomSchema = mongoose.Schema({
  room_number: String,
  room_type: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'roomtype'
  },
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  create_date: {
    type: Date,
    default: Date.now
  },
  updated_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  updated_date: Date, 
})

module.exports = mongoose.model('room', RoomSchema )
