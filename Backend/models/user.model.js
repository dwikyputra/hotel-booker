var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
    name: {
      type: String,
      required: true
    },
    email: {
      type: String,
      required: true
    },
    role: {
      type: String,
      required: true,
      enum: ['admin', 'member',],
    },
    password: String,
    gender: String,
    phone: String,
    created_by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user'
    },
    create_date: {
      type: Date,
      default: Date.now
    },
    updated_by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user'
    },
    updated_date: Date, 
});

module.exports = mongoose.model('user', userSchema);