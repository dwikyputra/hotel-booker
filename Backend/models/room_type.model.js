const mongoose = require('mongoose');

let RoomTypeSchema = mongoose.Schema({
  name: String,
  subtitle: String,
  description: String,
  image: String,
  price: Number,
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  create_date: {
    type: Date,
    default: Date.now
  },
  updated_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  updated_date: Date, 
})

module.exports = mongoose.model('roomtype', RoomTypeSchema )
