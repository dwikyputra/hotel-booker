let express = require('express');
let bodyParser = require('body-parser');
let mongoose = require('mongoose');
let logger = require('morgan');
let cors = require('cors');
let session = require('express-session');
var path = require('path');

// Initialise the app
let app = express();

// Import routes
let apiRoutes = require("./routes/router");

// Connect to Mongoose and set connection variable
mongoose.connect('mongodb://localhost/resthotel', { useNewUrlParser: true});
var db = mongoose.connection;
if(!db) console.log("Error connecting db")
else console.log("Db connected successfully")

// setup cors 
app.use(cors());

// Configure bodyparser to handle post requests
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

// App Use
app.use('/api', apiRoutes);
app.use(express.json())
app.use(logger('tiny'));
app.use(express.static('assets'));
app.use(session({
  key: 'hotelbooker098',
  secret: '7A5A17D9F5B098C1FCA768CDD7543773',
  resave: false,
  saveUninitialized: false,
  cookie: {
    expires: 600000
  }
}));

// frontend static
app.use(express.static(path.join(__dirname, 'client/build')));
app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
});

// Setup server port
var port = process.env.PORT || 4000;
app.listen(port, function () {
    console.log("Running RestHub on port " + port);
});